#ifndef UNDERTOW_STRINGOPS_H
#define UNDERTOW_STRINGOPS_H

#include <glib.h>

gchar *
undertow_string_clean (const gchar *dirty);

gchar *
undertow_string_urlencode (const gchar *dirty);

#endif
