#ifndef UNDERTOW_MAIN_WINDOW_H
#define UNDERTOW_MAIN_WINDOW_H

#include <gtk/gtk.h>

GtkWidget *
undertow_main_window_create (void);

void
undertow_main_window_destroy (void);

#endif
