#ifndef UNDERTOW_CHANNEL_VIEW_H
#define UNDERTOW_CHANNEL_VIEW_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <goocanvas.h>
#include <time.h>
#include <unistd.h>

#include "undertow-channel.h"

#define UNDERTOW_TYPE_CHANNEL_VIEW             (undertow_channel_view_get_type())
#define UNDERTOW_CHANNEL_VIEW(obj)             \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), UNDERTOW_TYPE_CHANNEL_VIEW, UndertowChannelView))
#define UNDERTOW_CHANNEL_VIEW_CLASS(klass)     \
    (G_TYPE_CHECK_CLASS_CAST((klass), UNDERTOW_TYPE_CHANNEL_VIEW, UndertowChannelViewClass))
#define UNDERTOW_IS_CHANNEL_VIEW(obj)          \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), UNDERTOW_TYPE_CHANNEL_VIEW))
#define UNDERTOW_IS_CHANNEL_VIEW_CLASS(klass)  \
    (G_TYPE_CHECK_CLASS_TYPE((klass), UNDERTOW_TYPE_CHANNEL_VIEW))
#define UNDERTOW_CHANNEL_VIEW_GET_CLASS(obj)   \
    (G_TYPE_INSTANCE_GET_CLASS((obj), UNDERTOW_TYPE_CHANNEL_VIEW, UndertowChannelViewClass))

typedef struct {
    GooCanvas parent;
    UndertowChannel *channel;
    GooCanvasItem *root_item;
    GList *items;
    guint items_height;
    guint canvas_width;
    gboolean disposed;
} UndertowChannelView;

typedef struct {
    GooCanvasClass parent_class;
} UndertowChannelViewClass;

GType undertow_channel_view_get_type (void);

UndertowChannelView *
undertow_channel_view_new_with_model (UndertowChannel *channel);

#endif
