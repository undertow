#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <unistd.h>
#include <string.h>
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <libundertow.h>

#include "undertow-location-model.h"
#include "undertow-channel.h"
#include "undertow-episode.h"

static GtkTreeStoreClass *parent_class = NULL;

static void
display_error_dialog (const gchar *brief, const gchar *detail)
{
    g_critical ("%s: %s", brief, detail);
}

static gboolean
find_channel (UndertowLocationModel *       model,
              const gchar *                 url,
              GtkTreeIter *                 iter)
{
    do {
        GtkTreeIter child;
        gchar *curr;
        gboolean is_curr = FALSE;

        gtk_tree_model_get (GTK_TREE_MODEL (model), iter,
                            UNDERTOW_LOCATION_MODEL_URL_COLUMN, &curr,
                            -1);
        if (curr != NULL && !strcmp (curr, url))
            is_curr = TRUE;
        if (curr != NULL)
            g_free (curr);
        if (is_curr)
            return TRUE;
        if (gtk_tree_model_iter_has_child (GTK_TREE_MODEL (model), iter)) {
            gtk_tree_model_iter_children (GTK_TREE_MODEL (model), &child, iter);
            if (find_channel (model, url, &child)) {
                *iter = child;
                return TRUE;
            }
        }

    } while (gtk_tree_model_iter_next (GTK_TREE_MODEL (model), iter));
    return FALSE;
}

static void
added_channel (UndertowChannelManager *     manager,
               UndertowChannel *            channel,
               UndertowLocationModel *      model)
{
    GtkTreeIter iter;
    gchar *url, *title;

    /* find the row corresponding to the channel */
    g_object_get (channel, "url", &url, "title", &title, NULL);
    gtk_tree_model_get_iter_first (GTK_TREE_MODEL (model), &iter);
    if (find_channel (model, url, &iter))
        gtk_tree_store_set (GTK_TREE_STORE (model), &iter,
                            UNDERTOW_LOCATION_MODEL_TITLE_COLUMN, title,
                            -1);
    else
        display_error_dialog ("Failed to add channel", "Couldn't find channel");
    if (url)
        g_free (url);
    if (title)
        g_free (title);
}

static void
channel_error (UndertowChannelManager *     manager,
               const gchar *                url,
               GError *                     error,
               UndertowLocationModel *      model)
{
    display_error_dialog (url, error->message);
}

static void
load_folder (UndertowLocationModel *    model,
             xmlNodePtr                 node,
             GtkTreeIter *              parent)
{
    GtkTreeIter iter;
    gchar *markup;

    while (node != NULL) {

        /* load channel */
        if (node->type == XML_ELEMENT_NODE &&
            xmlStrEqual (node->name, BAD_CAST "channel")) {
            UndertowChannel *channel;
            xmlChar *url;

            url = xmlGetProp (node, BAD_CAST "url");

            channel = undertow_channel_manager_get_channel (model->channels, (gchar *)url);
            if (channel != NULL) {
                xmlChar *title;

                title = xmlGetProp (node, BAD_CAST "title");
                markup = g_markup_escape_text ((gchar *)title, -1);
                xmlFree (title);

                gtk_tree_store_append (GTK_TREE_STORE (model), &iter, parent);
                gtk_tree_store_set (GTK_TREE_STORE (model), &iter,
                    UNDERTOW_LOCATION_MODEL_TYPE_COLUMN, UNDERTOW_LOCATION_TYPE_CHANNEL,
                    UNDERTOW_LOCATION_MODEL_URL_COLUMN, url,
                    UNDERTOW_LOCATION_MODEL_TITLE_COLUMN, markup,
                    -1);
                g_free (markup);
            }
            if (url)
                xmlFree (url);
        }

        /* load folder */
        if (node->type == XML_ELEMENT_NODE &&
            xmlStrEqual (node->name, BAD_CAST "folder")) {
            xmlChar *title;

            title = xmlGetProp (node, BAD_CAST "title");
            gtk_tree_store_append (GTK_TREE_STORE (model), &iter, parent);
            gtk_tree_store_set (GTK_TREE_STORE (model), &iter,
                UNDERTOW_LOCATION_MODEL_TYPE_COLUMN, UNDERTOW_LOCATION_TYPE_FOLDER,
                UNDERTOW_LOCATION_MODEL_TITLE_COLUMN, title,
                -1);
            xmlFree (title);

            load_folder (model, node->children, &iter);
        }

        /* load playlist */
        if (node->type == XML_ELEMENT_NODE &&
            xmlStrEqual (node->name, BAD_CAST "playlist")) {
        }

        node = node->next;
    }
}

static void
load_locations (UndertowLocationModel *model)
{
    gchar *locations_path;
    xmlDocPtr doc;
    xmlNodePtr node;

    locations_path = g_build_path ("/",
                                   g_get_home_dir (),
                                   ".undertow",
                                   "locations.xml",
                                   NULL);
    doc = xmlReadFile (locations_path, NULL, XML_PARSE_NOENT);
    g_free (locations_path);
    if (doc == NULL) {
        g_critical ("failed to load locations.xml");
        return;
    }
    node = xmlDocGetRootElement (doc);
    while (node != NULL) {
        if (node->type == XML_ELEMENT_NODE &&
            xmlStrEqual (node->name, BAD_CAST "locations")) {
            load_folder (model, node->children, NULL);
            xmlFreeDoc (doc);
            return;
        }
        node = node->next;
    }
    g_critical ("failed to load locations.xml: root element is not 'locations'");
    xmlFreeDoc (doc);
}

static void
load_orphaned_channel (UndertowChannelManager *     manager,
                       UndertowChannel *            channel,
                       UndertowLocationModel *      model)
{
    gchar *url, *title;
    GtkTreeIter iter;

    g_object_get (channel, "url", &url, "title", &title, NULL);
    gtk_tree_model_get_iter_first (GTK_TREE_MODEL (model), &iter);
    if (!find_channel (model, url, &iter)) {
        gtk_tree_store_append (GTK_TREE_STORE (model), &iter, NULL);
        gtk_tree_store_set (GTK_TREE_STORE (model), &iter,
            UNDERTOW_LOCATION_MODEL_TYPE_COLUMN, UNDERTOW_LOCATION_TYPE_CHANNEL,
            UNDERTOW_LOCATION_MODEL_URL_COLUMN, url,
            UNDERTOW_LOCATION_MODEL_TITLE_COLUMN, title,
            -1);
        g_debug ("loaded orphaned channel %s", title);
    }
    g_free (url);
    g_free (title);
}

static void
model_init (UndertowLocationModel *             model,
            UndertowLocationModelClass *        klass)
{
    GType column_types[] = { G_TYPE_INT, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING };

    model->channels = undertow_channel_manager_get ();
    g_signal_connect (model->channels, "channel-added", G_CALLBACK (added_channel), model);
    g_signal_connect (model->channels, "channel-error", G_CALLBACK (channel_error), model);
    gtk_tree_store_set_column_types (GTK_TREE_STORE (model),
                                     G_N_ELEMENTS (column_types),
                                     column_types);
    load_locations (model);
    undertow_channel_manager_foreach_channel (model->channels,
        (UndertowChannelManagerForeachFunc)load_orphaned_channel,
        model);
}

static GObject *
model_constructor (GType                        type,
                   guint                        n_props,
                   GObjectConstructParam *      props)
{
    UndertowLocationModelClass *model_class;
    GObjectClass *parent_class;

    model_class = UNDERTOW_LOCATION_MODEL_CLASS (g_type_class_peek (type));
    if (!model_class->singleton) {
        parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (model_class));
        model_class->singleton = parent_class->constructor (type, n_props, props);
    }
    else
        g_object_ref (model_class->singleton);
    return model_class->singleton;
}

static void
dump_location (UndertowLocationModel *      model,
               GtkTreeIter *                iter,
               gint                         depth,
               FILE *                       fp)
{

    do {
        gchar *url, *title, *markup;
        UndertowLocationType type;
        GtkTreeIter child;
        int i;

        gtk_tree_model_get (GTK_TREE_MODEL (model), iter,
                            UNDERTOW_LOCATION_MODEL_TYPE_COLUMN, &type,
                            UNDERTOW_LOCATION_MODEL_URL_COLUMN, &url,
                            UNDERTOW_LOCATION_MODEL_TITLE_COLUMN, &title,
                            -1);
        switch (type) {
            case UNDERTOW_LOCATION_TYPE_CHANNEL:
                markup = g_markup_escape_text (url, -1);
                for (i = 0; i < depth; i++)
                    fprintf (fp, "    "); 
                fprintf (fp, "<channel title=\"%s\" url=\"%s\" />\n", title, markup);
                if (markup)
                    g_free (markup);
                break;
            case UNDERTOW_LOCATION_TYPE_FOLDER:
                for (i = 0; i < depth; i++)
                    fprintf (fp, "    "); 
                fprintf (fp, "<folder title=\"%s\">\n", title);
                if (gtk_tree_model_iter_children (GTK_TREE_MODEL (model), &child, iter))
                    dump_location (model, &child, depth + 1, fp);
                for (i = 0; i < depth; i++)
                    fprintf (fp, "    "); 
                fprintf (fp, "</folder>\n");
                break;
            case UNDERTOW_LOCATION_TYPE_PLAYLIST:
                break;
            default:
                g_warning ("failed to dump location (title=%s, url=%s): no type", title, url);
                break;
        }
        if (url)
            g_free (url);
        if (title)
            g_free (title);
    } while (gtk_tree_model_iter_next (GTK_TREE_MODEL (model), iter));
}

static void
model_dispose (UndertowLocationModel *model)
{
    gchar *locations_path;
    FILE *fp;
    GtkTreeIter iter;

    if (model->disposed)
        return;
    g_debug ("location_model_dispose");
    locations_path = g_build_path ("/",
                                   g_get_home_dir (),
                                   ".undertow",
                                   "locations.xml",
                                   NULL);
    fp = fopen (locations_path, "w");
    fprintf (fp, "<?xml version=\"1.0\"?>\n");
    fprintf (fp, "<locations>\n");
    if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL (model), &iter))
        dump_location (model, &iter, 1, fp);
    fprintf (fp, "</locations>\n");
    fclose (fp);
    G_OBJECT_CLASS (parent_class)->dispose (G_OBJECT (model));
    g_object_unref (model->channels);
    model->disposed = TRUE;
}

static void
model_class_init (UndertowLocationModelClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    parent_class = g_type_class_peek_parent (klass);

    object_class->constructor = model_constructor;
    object_class->dispose = (GObjectFinalizeFunc) model_dispose;
}

GType
undertow_location_model_get_type (void)
{
    static GType type = 0;

    if (!type) {
        static const GTypeInfo model_info = {
            sizeof (UndertowLocationModelClass),
            NULL,
            NULL,
            (GClassInitFunc) model_class_init,
            NULL,
            NULL,
            sizeof (UndertowLocationModel),
            0,
            (GInstanceInitFunc) model_init,
            NULL
        };
        type = g_type_register_static (GTK_TYPE_TREE_STORE, 
                                       "UndertowLocationModel",
                                       &model_info,
                                       0);
    }
    return type;
}

UndertowLocationModel *
undertow_location_model_get (void)
{
    return UNDERTOW_LOCATION_MODEL (g_object_new (UNDERTOW_TYPE_LOCATION_MODEL, NULL));
}

enum {
    FOLDER_CHOOSER_PATH_COLUMN,
    FOLDER_CHOOSER_TITLE_COLUMN,
    FOLDER_CHOOSER_N_COLUMNS
};

static void
load_folders (GtkTreeStore *    dst_store,
              GtkTreeModel *    src_model,
              GtkTreeIter *     src,
              GtkTreeIter *     dst_parent)
{
    GtkTreeIter dst, src_child;

    do {
        gchar *title;
        UndertowLocationType type;

        gtk_tree_model_get (src_model, src,
                            UNDERTOW_LOCATION_MODEL_TYPE_COLUMN, &type,
                            UNDERTOW_LOCATION_MODEL_TITLE_COLUMN, &title,
                            -1);
        if (type == UNDERTOW_LOCATION_TYPE_FOLDER) {
            GtkTreePath *path;
            gchar *path_string;

            path = gtk_tree_model_get_path (src_model, src);
            path_string = gtk_tree_path_to_string (path);
            gtk_tree_path_free (path);
            gtk_tree_store_append (dst_store, &dst, dst_parent);
            gtk_tree_store_set (dst_store, &dst,
                                FOLDER_CHOOSER_PATH_COLUMN, path_string,
                                FOLDER_CHOOSER_TITLE_COLUMN, title,
                                -1);
            g_debug ("load_folders: %s at %s", title, path_string);
            g_free (path_string);
            if (gtk_tree_model_iter_children (src_model, &src_child, src))
                load_folders (dst_store, src_model, &src_child, &dst);
        }
        g_free (title);
    }
    while (gtk_tree_model_iter_next (src_model, src));
}

static GtkWidget *
folder_chooser_new (UndertowLocationModel *model)
{
    GtkTreeStore *store;
    GtkWidget *view;
    GtkCellRenderer *folder_title;
    GtkTreeViewColumn *folder_title_column;
    GtkTreeIter iter, src;

    /* create the folder view */
    store = gtk_tree_store_new (FOLDER_CHOOSER_N_COLUMNS, G_TYPE_STRING, G_TYPE_STRING);
    view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (store));
    gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (view), FALSE);
    gtk_tree_view_set_reorderable (GTK_TREE_VIEW (view), FALSE);

    folder_title = gtk_cell_renderer_text_new ();
    folder_title_column =
        gtk_tree_view_column_new_with_attributes ("Title",
                                                  folder_title,
                                                  "markup", FOLDER_CHOOSER_TITLE_COLUMN,
                                                  NULL);
    gtk_tree_view_append_column (GTK_TREE_VIEW (view), folder_title_column);

    /* populate the folder model */
    gtk_tree_store_append (GTK_TREE_STORE (store), &iter, NULL);
    gtk_tree_store_set (store, &iter,
                        FOLDER_CHOOSER_PATH_COLUMN, NULL,
                        FOLDER_CHOOSER_TITLE_COLUMN, "<b>Channels</b>",
                        -1);
    if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL (model), &src))
        load_folders (store, GTK_TREE_MODEL (model), &src, &iter);
    gtk_tree_view_expand_all (GTK_TREE_VIEW (view));
    return view;
}

static void
on_dialog_activate (GtkEntry *entry, GtkDialog *dialog)
{
    gtk_dialog_response (dialog, GTK_RESPONSE_OK);
}

void
undertow_location_model_add_channel (UndertowLocationModel *model)
{
    GtkWidget *dialog, *frame, *chooser, *label, *entry;
    const gchar *url;
    gchar *path_string, *markup;
    GtkTreeIter iter, piter, *piter_ptr;
    GtkTreeSelection *selection;
    GtkTreeModel *folders;
    GError *error = NULL;

    /* create the New Channel dialog */
    dialog = gtk_dialog_new_with_buttons ("New Channel...",
                                          NULL,
                                          GTK_DIALOG_DESTROY_WITH_PARENT,
                                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                          GTK_STOCK_OK, GTK_RESPONSE_OK,
                                          NULL);
    gtk_window_set_resizable (GTK_WINDOW (dialog), TRUE);
    label = gtk_label_new (NULL);
    gtk_label_set_markup (GTK_LABEL (label), "<b>New Channel</b>\nEnter the URL to a valid RSS feed");
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_misc_set_padding (GTK_MISC (label), 12, 12);
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), label, FALSE, FALSE, 0);
    entry = gtk_entry_new ();
    gtk_entry_set_text (GTK_ENTRY (entry), "http://");
    gtk_entry_set_width_chars (GTK_ENTRY (entry), 40);
    g_signal_connect (entry, "activate", G_CALLBACK (on_dialog_activate), dialog);
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), entry, FALSE, FALSE, 6);
    chooser = folder_chooser_new (model);
    gtk_widget_set_size_request (chooser, 400, 300);
    frame = gtk_frame_new (NULL);
    gtk_container_add (GTK_CONTAINER (frame), chooser);
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), frame, TRUE, TRUE, 6);

    /* display the dialog */
    gtk_widget_show_all (dialog);
    if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK) {
        url = gtk_entry_get_text (GTK_ENTRY (entry));
        selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (chooser));
        piter_ptr = NULL;   /* default is add to the root of the model */
        if (gtk_tree_selection_get_selected (selection, &folders, &iter)) {
            gtk_tree_model_get (folders, &iter, FOLDER_CHOOSER_PATH_COLUMN, &path_string, -1);
            if (path_string != NULL) {
                gtk_tree_model_get_iter_from_string (folders, &piter, path_string);
                piter_ptr = &piter;
                g_free (path_string);
            }
        }

        /* add the channel */
        undertow_channel_manager_add_channel (model->channels, url, &error);
        if (error != NULL) {
            display_error_dialog ("Failed to add channel", error->message);
            g_free (path_string);
            g_error_free (error);
            return;
        }
        markup = g_strdup_printf ("<i>%s</i>", url);
        gtk_tree_store_append (GTK_TREE_STORE (model), &iter, piter_ptr);
        gtk_tree_store_set (GTK_TREE_STORE (model), &iter,
            UNDERTOW_LOCATION_MODEL_TYPE_COLUMN, UNDERTOW_LOCATION_TYPE_CHANNEL,
            UNDERTOW_LOCATION_MODEL_TITLE_COLUMN, markup,
            UNDERTOW_LOCATION_MODEL_URL_COLUMN, url,
            -1);
        g_free (markup);
    }
    gtk_widget_destroy (dialog);
}

void
undertow_location_model_add_folder (UndertowLocationModel *model)
{
    GtkWidget *dialog, *frame, *chooser, *label, *entry;
    const gchar *name;
    gchar *path_string;
    GtkTreeIter iter, piter, *piter_ptr;
    GtkTreeSelection *selection;
    GtkTreeModel *folders;

    /* create the New Folder dialog */
    dialog = gtk_dialog_new_with_buttons ("New Folder...",
                                          NULL,
                                          GTK_DIALOG_DESTROY_WITH_PARENT,
                                          GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
                                          GTK_STOCK_OK, GTK_RESPONSE_OK,
                                          NULL);
    gtk_window_set_resizable (GTK_WINDOW (dialog), TRUE);
    label = gtk_label_new (NULL);
    gtk_label_set_markup (GTK_LABEL (label), "<b>New Folder</b>\nEnter a name for the folder");
    gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
    gtk_misc_set_padding (GTK_MISC (label), 12, 12);
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), label, FALSE, FALSE, 0);
    entry = gtk_entry_new ();
    gtk_entry_set_text (GTK_ENTRY (entry), "New Folder");
    gtk_entry_set_width_chars (GTK_ENTRY (entry), 40);
    g_signal_connect (entry, "activate", G_CALLBACK (on_dialog_activate), dialog);
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), entry, FALSE, FALSE, 6);
    chooser = folder_chooser_new (model);
    gtk_widget_set_size_request (chooser, 400, 300);
    frame = gtk_frame_new (NULL);
    gtk_container_add (GTK_CONTAINER (frame), chooser);
    gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), frame, TRUE, TRUE, 6);

    /* display the dialog */
    gtk_widget_show_all (dialog);
    if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK) {
        name = gtk_entry_get_text (GTK_ENTRY (entry));
        selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (chooser));
        piter_ptr = NULL;   /* default is add to the root of the model */
        if (gtk_tree_selection_get_selected (selection, &folders, &iter)) {
            gtk_tree_model_get (folders, &iter, FOLDER_CHOOSER_PATH_COLUMN, &path_string, -1);
            if (path_string != NULL) {
                gtk_tree_model_get_iter_from_string (folders, &piter, path_string);
                piter_ptr = &piter;
                g_free (path_string);
            }
        }

        /* add the channel */
        gtk_tree_store_append (GTK_TREE_STORE (model), &iter, piter_ptr);
        gtk_tree_store_set (GTK_TREE_STORE (model), &iter,
            UNDERTOW_LOCATION_MODEL_TYPE_COLUMN, UNDERTOW_LOCATION_TYPE_FOLDER,
            UNDERTOW_LOCATION_MODEL_TITLE_COLUMN, name,
            -1);
    }
    gtk_widget_destroy (dialog);
}

void
undertow_location_model_add_playlist (UndertowLocationModel *model)
{
    g_debug ("add playlist");
}

void
undertow_location_model_move (UndertowLocationModel *model,
                              GtkTreeIter *src_iter,
                              GtkTreeIter *dest_iter,
                              GtkTreeViewDropPosition position)
{
    gchar *url, *title;
    UndertowLocationType type, dest_type;
    GtkTreeIter new;

    g_return_if_fail (src_iter != NULL);

    gtk_tree_model_get (GTK_TREE_MODEL (model), src_iter,
                        UNDERTOW_LOCATION_MODEL_TYPE_COLUMN, &type,
                        UNDERTOW_LOCATION_MODEL_URL_COLUMN, &url,
                        UNDERTOW_LOCATION_MODEL_TITLE_COLUMN, &title,
                        -1);
    if (dest_iter == NULL)
        gtk_tree_store_append (GTK_TREE_STORE (model), &new, NULL);
    else {
        gtk_tree_model_get (GTK_TREE_MODEL (model), dest_iter,
                            UNDERTOW_LOCATION_MODEL_TYPE_COLUMN, &dest_type,
                            -1);
        switch (position) {
            case GTK_TREE_VIEW_DROP_BEFORE:
                gtk_tree_store_insert_before (GTK_TREE_STORE (model),
                                              &new,
                                              NULL,
                                              dest_iter);
                break;
            case GTK_TREE_VIEW_DROP_AFTER:
                gtk_tree_store_insert_after (GTK_TREE_STORE (model),
                                             &new,
                                             NULL,
                                             dest_iter);
                break;
            case GTK_TREE_VIEW_DROP_INTO_OR_BEFORE:
                if (dest_type == UNDERTOW_LOCATION_TYPE_FOLDER)
                    gtk_tree_store_append (GTK_TREE_STORE (model),
                                           &new,
                                           dest_iter);
                else
                    gtk_tree_store_insert_before (GTK_TREE_STORE (model),
                                                  &new,
                                                  NULL,
                                                  dest_iter);
                break;
            case GTK_TREE_VIEW_DROP_INTO_OR_AFTER:
                if (dest_type == UNDERTOW_LOCATION_TYPE_FOLDER)
                    gtk_tree_store_append (GTK_TREE_STORE (model),
                                           &new,
                                           dest_iter);
                else
                    gtk_tree_store_insert_after (GTK_TREE_STORE (model),
                                                 &new,
                                                 NULL,
                                                 dest_iter);
                break;
        }
    }

    gtk_tree_store_set (GTK_TREE_STORE (model), &new,
                        UNDERTOW_LOCATION_MODEL_TYPE_COLUMN, type,
                        UNDERTOW_LOCATION_MODEL_URL_COLUMN, url,
                        UNDERTOW_LOCATION_MODEL_TITLE_COLUMN, title,
                        -1);
    gtk_tree_store_remove (GTK_TREE_STORE (model), src_iter);
    if (url)
        g_free (url);
    if (title)
        g_free (title);
}
