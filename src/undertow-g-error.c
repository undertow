#include <glib.h>

GQuark
undertow_g_error_quark (void)
{
    return g_quark_from_static_string ("undertow-g-error-quark");
}
