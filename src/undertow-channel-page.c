#include <gtk/gtk.h>

#include "undertow-channel-page.h"
#include "undertow-channel.h"
#include "undertow-channel-view.h"
#include "undertow-channel-manager.h"
#include "undertow-episode.h"
#include "undertow-reflow-label.h"

enum { PAGE_URL = 1 };

GtkVBoxClass *parent_class = NULL;

static void
channel_page_get_property (UndertowChannelPage *        page,
                           guint                        prop_id,
                           GValue *                     value,
                           GParamSpec *                 spec)
{
    switch (prop_id) {
        case PAGE_URL:
            g_value_set_string (value, page->channel_url);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (G_OBJECT (page), prop_id, spec);
            break;
    }
}

static void 
channel_page_set_property (UndertowChannelPage *        page,
                           guint                        prop_id,
                           const GValue *               value,
                           GParamSpec *                 spec)
{
    UndertowChannelManager *channels;
    GtkWidget *align, *hbox, *sw;
    gchar *title, *desc, *markup;

    switch (prop_id) {
        case PAGE_URL:
            page->channel_url = g_value_dup_string (value);
            channels = undertow_channel_manager_get ();
            page->channel = undertow_channel_manager_get_channel (channels, page->channel_url);
            g_object_unref (channels);
            /* build the header area */
            /*
            align = gtk_alignment_new (0.5, 0.5, 1.0, 1.0);
            gtk_alignment_set_padding (GTK_ALIGNMENT (align), 12, 12, 12, 12);
            gtk_box_pack_start (GTK_BOX (page), align, FALSE, FALSE, 6);
            hbox = gtk_hbox_new (FALSE, 0);
            g_object_get (page->channel, "title", &title, "description", &desc, NULL);
            markup = g_strdup_printf ("<span weight='bold' size='large'>%s</span>\n\n%s", title, desc);
            page->title = undertow_reflow_label_new_with_markup (markup);
            page->title = gtk_label_new (NULL);
            gtk_label_set_markup (GTK_LABEL (page->title), markup);
            g_free (title);
            g_free (desc);
            g_free (markup);
            gtk_box_pack_start (GTK_BOX (hbox), page->title, TRUE, TRUE, 12);
            page->image = gtk_image_new_from_stock (GTK_STOCK_FILE, GTK_ICON_SIZE_LARGE_TOOLBAR);
            gtk_box_pack_start (GTK_BOX (hbox), page->image, FALSE, FALSE, 0);
            gtk_container_add (GTK_CONTAINER (align), hbox);
            */
            /* add the separator */
            /*
            gtk_box_pack_start (GTK_BOX (page), gtk_hseparator_new (), FALSE, FALSE, 0);
            */
            /* add the channel view */
            sw = gtk_scrolled_window_new (NULL, NULL);
            gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
                                            GTK_POLICY_NEVER,
                                            GTK_POLICY_AUTOMATIC);
            page->view = undertow_channel_view_new_with_model (page->channel);
            gtk_container_add (GTK_CONTAINER (sw), GTK_WIDGET (page->view));
            gtk_box_pack_start (GTK_BOX (page), sw, TRUE, TRUE, 0);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (G_OBJECT (page), prop_id, spec);
            break;
    }
}

static void
channel_page_dispose (UndertowChannelPage *page)
{
    if (page->disposed)
        return;
    g_debug ("channel_page_dispose");
    G_OBJECT_CLASS (parent_class)->dispose (G_OBJECT (page));
    g_object_unref (page->channel);
    page->disposed = TRUE;
}

static void
channel_page_class_init (UndertowChannelPageClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GParamSpec *pspec;

    parent_class = g_type_class_peek_parent (klass);

    object_class->get_property =
        (GObjectGetPropertyFunc) channel_page_get_property;
    object_class->set_property =
        (GObjectSetPropertyFunc) channel_page_set_property;
    object_class->dispose =
        (GObjectFinalizeFunc) channel_page_dispose;

    pspec = g_param_spec_string ("url", "url", "Get/set the channel url",
                                 NULL,
                                 G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY);
    g_object_class_install_property (object_class, PAGE_URL, pspec);
}

GType 
undertow_channel_page_get_type (void)
{
    static GType type = 0;

    if (!type) {
        static const GTypeInfo info = {
            sizeof (UndertowChannelPageClass),
            NULL,
            NULL,
            (GClassInitFunc) channel_page_class_init,
            NULL,
            NULL,
            sizeof (UndertowChannelPage),
            0,
            (GInstanceInitFunc) NULL,
            NULL
        };
        type = g_type_register_static (GTK_TYPE_VBOX, 
                                       "UndertowChannelPage",
                                       &info, 0);
    }
    return type;
}

GtkWidget *
undertow_channel_page_new (const gchar *url)
{
    return g_object_new (UNDERTOW_TYPE_CHANNEL_PAGE, "url", url, NULL);
}
