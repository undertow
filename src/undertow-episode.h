#ifndef UNDERTOW_EPISODE_H
#define UNDERTOW_EPISODE_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <time.h>
#include <unistd.h>
#include <libundertow.h>

#include "undertow-marshallers.h"

#define UNDERTOW_TYPE_EPISODE             (undertow_episode_get_type())
#define UNDERTOW_EPISODE(obj)             \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), UNDERTOW_TYPE_EPISODE, UndertowEpisode))
#define UNDERTOW_EPISODE_CLASS(klass)     \
    (G_TYPE_CHECK_CLASS_CAST((klass), UNDERTOW_TYPE_EPISODE, UndertowEpisodeClass))
#define UNDERTOW_IS_EPISODE(obj)          \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), UNDERTOW_TYPE_EPISODE))
#define UNDERTOW_IS_EPISODE_CLASS(klass)  \
    (G_TYPE_CHECK_CLASS_TYPE((klass), UNDERTOW_TYPE_EPISODE))
#define UNDERTOW_EPISODE_GET_CLASS(obj)   \
    (G_TYPE_INSTANCE_GET_CLASS((obj), UNDERTOW_TYPE_EPISODE, UndertowEpisodeClass))

typedef enum {
    UNDERTOW_DOWNLOAD_STATE_NOT_RUNNING,
    UNDERTOW_DOWNLOAD_STATE_RUNNING,
    UNDERTOW_DOWNLOAD_STATE_FINISHED
} UndertowDownloadState;

typedef struct {
    GObject parent;
    gboolean disposed;
    gchar *channel_url;
    gchar *content_url;
    gchar *title;
    gchar *description;
    time_t published;
    gchar *file_path;
    gsize file_size;
    gchar *file_type;
    lut_download *download;
} UndertowEpisode;

typedef struct {
    GObjectClass parent_class;
    void (*download_progress)(UndertowEpisode *, guint, guint, gfloat, gfloat);
    void (*download_error)(UndertowEpisode *, GError *);
    void (*state_changed)(UndertowEpisode *, UndertowDownloadState);
} UndertowEpisodeClass;

GType undertow_episode_get_type (void);

UndertowDownloadState
undertow_episode_get_state (UndertowEpisode *episode);

gboolean
undertow_episode_start_download (UndertowEpisode *episode, GError **error);

void
undertow_episode_stop_download (UndertowEpisode *episode, GError **error);

void
undertow_episode_cancel_download (UndertowEpisode *episode, GError **error);

#endif
