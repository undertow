#ifndef UNDERTOW_G_ERROR_H
#define UNDERTOW_G_ERROR_H

#include <glib.h>

#define UNDERTOW_G_ERROR    undertow_g_error_quark ()

GQuark
undertow_g_error_quark (void);

#endif
