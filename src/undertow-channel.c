#include <glib.h>
#include <glib-object.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libundertow.h>

#include "undertow-channel.h"
#include "undertow-stringops.h"
#include "undertow-episode.h"

enum {
    CHANNEL_TITLE = 1,
    CHANNEL_URL,
    CHANNEL_DESCRIPTION,
    CHANNEL_LAST_UPDATED
};

enum {
    CHANNEL_UPDATED_SIGNAL,
    CHANNEL_ADDED_EPISODE_SIGNAL,
    CHANNEL_EPISODE_EXPIRED_SIGNAL,
    N_CHANNEL_SIGNALS
};
static guint channel_signals[N_CHANNEL_SIGNALS];

static GObjectClass *parent_class = NULL;

/* sort episodes newest first */
static gint
sort_episodes (gconstpointer p1, gconstpointer p2)
{
    time_t t1, t2;
    UndertowEpisode *e1 = UNDERTOW_EPISODE (p1);
    UndertowEpisode *e2 = UNDERTOW_EPISODE (p2);

    g_object_get (e1, "published", &t1, NULL);
    g_object_get (e2, "published", &t2, NULL);
    if (t1 > t2)
        return -1;
    if (t1 < t2)
        return 1;
    return 0;
}

static void
load_episode (lut_episode *e, UndertowChannel *channel)
{
    gchar *clean_description;
    UndertowEpisode *episode;
   
    clean_description = undertow_string_clean (e->description);
    episode = g_object_new (UNDERTOW_TYPE_EPISODE,
                            "channel-url", channel->url,
                            "content-url", e->content_url,
                            "title", e->title,
                            "description", clean_description,
                            "published", e->published_on,
                            NULL);
    g_free (clean_description);
    lut_episode_free (e);
    channel->episodes = g_slist_insert_sorted (channel->episodes, episode, sort_episodes);
}

static void
channel_get_property (UndertowChannel *         channel,
                      guint                     property_id,
                      GValue *                  value,
                      GParamSpec *              spec)
{
    switch (property_id) {
        case CHANNEL_TITLE:
            g_value_set_string (value, channel->title);
            break;
        case CHANNEL_URL:
            g_value_set_string (value, channel->url);
            break;
        case CHANNEL_DESCRIPTION:
            g_value_set_string (value, channel->description);
            break;
        case CHANNEL_LAST_UPDATED:
            g_value_set_uint (value, channel->last_updated);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (G_OBJECT (channel), property_id, spec);
            break;
    }
}

static void 
channel_set_property (UndertowChannel *         channel,
                      guint                     property_id,
                      const GValue *            value,
                      GParamSpec *              spec)
{
    lut_channel *c;
    lut_errno retval;

    switch (property_id) {
        case CHANNEL_URL:
            channel->url = g_value_dup_string (value);
            retval = lut_get_channel (channel->url, &c);
            if (retval == LUT_OK) {
                channel->title = g_strdup (c->title);
                channel->description = undertow_string_clean (c->description);
                channel->last_updated = c->updated_on;
                lut_foreach_episode (channel->url,
                                     (lut_foreach_episode_funct) load_episode,
                                     channel);
                lut_channel_free (c);
            }
            else
                g_critical ("failed to get channel %s: %s", channel->url, lut_strerror (retval));
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (G_OBJECT (channel), property_id, spec);
            break;
    }
}

static void
channel_dispose (UndertowChannel *channel)
{
    GSList *item;

    if (channel->disposed)
        return;
    g_debug ("channel_dispose: %s", channel->url);
    for (item = channel->episodes; item != NULL; item = g_slist_next (item))
        g_object_unref (UNDERTOW_EPISODE (item->data));
    g_slist_free (channel->episodes);
    if (channel->title)
        g_free (channel->title);
    if (channel->url)
        g_free (channel->url);
    if (channel->description)
        g_free (channel->description);
    channel->disposed = TRUE;
    G_OBJECT_CLASS (parent_class)->dispose (G_OBJECT (channel));
}

static void
channel_class_init (UndertowChannelClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GParamSpec *pspec;

    parent_class = g_type_class_peek_parent (klass);

    object_class->get_property =
        (GObjectGetPropertyFunc) channel_get_property;
    object_class->set_property =
        (GObjectSetPropertyFunc) channel_set_property;
    object_class->dispose =
        (GObjectFinalizeFunc) channel_dispose;

    pspec = g_param_spec_string ("url", "channel URL",
                                 "Get the channel URL",
                                 NULL,
                                 G_PARAM_READWRITE|G_PARAM_CONSTRUCT_ONLY);
    g_object_class_install_property (object_class, CHANNEL_URL, pspec);
    pspec = g_param_spec_string ("title", "channel title",
                                 "Get/set the channel title",
                                 NULL,
                                 G_PARAM_READABLE);
    g_object_class_install_property (object_class, CHANNEL_TITLE, pspec);
    pspec = g_param_spec_string ("description", "channel description",
                                 "Get/set the channel description",
                                 NULL,
                                 G_PARAM_READABLE);
    g_object_class_install_property (object_class, CHANNEL_DESCRIPTION, pspec);
    pspec = g_param_spec_uint ("last-updated", "channel modification time",
                               "Get/set the channel modification time",
                               0, G_MAXUINT, 0,
                               G_PARAM_READABLE);
    g_object_class_install_property (object_class, CHANNEL_LAST_UPDATED, pspec);


    channel_signals[CHANNEL_UPDATED_SIGNAL] =
      g_signal_new ("channel-updated",
                    G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST|G_SIGNAL_NO_RECURSE|G_SIGNAL_NO_HOOKS,
                    G_STRUCT_OFFSET (UndertowChannelClass, channel_updated),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__VOID,
                    G_TYPE_NONE, 0);
    channel_signals[CHANNEL_ADDED_EPISODE_SIGNAL] =
      g_signal_new ("episode-added",
                    G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST|G_SIGNAL_NO_RECURSE|G_SIGNAL_NO_HOOKS,
                    G_STRUCT_OFFSET (UndertowChannelClass, episode_added),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__OBJECT,
                    G_TYPE_NONE, 1, UNDERTOW_TYPE_EPISODE);
    channel_signals[CHANNEL_EPISODE_EXPIRED_SIGNAL] =
      g_signal_new ("episode-expired",
                    G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST|G_SIGNAL_NO_RECURSE|G_SIGNAL_NO_HOOKS,
                    G_STRUCT_OFFSET (UndertowChannelClass, episode_expired),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__OBJECT,
                    G_TYPE_NONE, 1, UNDERTOW_TYPE_EPISODE);
}

GType 
undertow_channel_get_type (void)
{
    static GType type = 0;

    if (!type) {
        static const GTypeInfo channel_info = {
            sizeof (UndertowChannelClass),
            NULL,
            NULL,
            (GClassInitFunc) channel_class_init,
            NULL,
            NULL,
            sizeof (UndertowChannel),
            0,
            (GInstanceInitFunc) NULL,
            NULL
        };
        type = g_type_register_static (G_TYPE_OBJECT, 
                                       "UndertowChannel",
                                       &channel_info, 0);
    }
    return type;
}

UndertowChannel *
undertow_channel_new (const gchar *url)
{
    UndertowChannel *channel;

    channel = g_object_new (UNDERTOW_TYPE_CHANNEL, "url", url, NULL);
    return UNDERTOW_CHANNEL (channel);
}

static void
on_episode_added (lut_episode *e, void *user_data)
{
    UndertowChannel *channel;
    UndertowEpisode *episode;
    gchar *clean_description;
   
    gdk_threads_enter ();

    g_debug ("episode-added: %s", e->content_url);
    channel = UNDERTOW_CHANNEL (user_data);
    clean_description = undertow_string_clean (e->description);
    episode = g_object_new (UNDERTOW_TYPE_EPISODE,
                            "channel", channel,
                            "content-url", e->content_url,
                            "title", e->title,
                            "description", clean_description,
                            "published", e->published_on,
                            NULL);
    g_free (clean_description);
    lut_episode_free (e);
    channel->episodes = g_slist_append (channel->episodes, episode);
    g_signal_emit (channel,
                   channel_signals[CHANNEL_ADDED_EPISODE_SIGNAL], 
                   0,
                   episode);

    gdk_flush ();
    gdk_threads_leave ();
}

static void
on_episode_expired (lut_episode *episode, void *user_data)
{
    lut_episode_free (episode);
}

static void
on_channel_updated (lut_channel *channel, void *user_data)
{
    lut_channel_free (channel);
}

static void
on_refresh_completed (lut_errno error, void *user_data)
{
    UndertowChannel *channel;

    gdk_threads_enter ();

    channel = UNDERTOW_CHANNEL (user_data);
    if (error != LUT_OK)
        g_debug ("lut_refresh_channel failed: %s", lut_strerror (error));
    g_object_unref (channel);

    gdk_flush ();
    gdk_threads_leave ();
}

void
undertow_channel_refresh (UndertowChannel *channel)
{
    int retval;

    if (channel->is_refreshing)
        return;
    g_object_ref (channel);
    retval = lut_refresh_channel (channel->url,
                                  on_channel_updated,
                                  on_episode_added,
                                  on_episode_expired,
                                  on_refresh_completed,
                                  channel);
    if (retval != LUT_OK) {
        g_critical ("undertow_channel_refresh: %s", lut_strerror (retval));
        g_object_unref (channel);
    }
    channel->is_refreshing = TRUE;
    g_debug ("undertow_channel_refresh: refreshing %s", channel->url);
}

void
undertow_channel_remove (UndertowChannel *channel)
{
    lut_delete_channel (channel->url);
}

UndertowEpisodeIter *
undertow_channel_get_episode_iter (UndertowChannel *channel)
{
    return (UndertowEpisodeIter *) channel->episodes;
}

UndertowEpisodeIter *
undertow_episode_iter_next (UndertowEpisodeIter *iter)
{
    if (!iter)
        return NULL;
    return (UndertowEpisodeIter *) ((GSList *)iter)->next;
}

UndertowEpisode *
undertow_episode_iter_get_episode (UndertowEpisodeIter *iter)
{
    if (!iter)
        return NULL;
    g_object_ref (((GSList *)iter)->data);
    return UNDERTOW_EPISODE (((GSList *)iter)->data);
}
