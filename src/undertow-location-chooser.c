#include <string.h>
#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <gconf/gconf-client.h>
#include <errno.h>

#include "undertow-location-chooser.h"
#include "undertow-location-model.h"

enum {
    LOCATION_CHANGED_SIGNAL,
    POPUP_CONTEXT_MENU_SIGNAL,
    N_SIGNALS
};
static guint signals[N_SIGNALS];

static GtkVBoxClass *parent_class = NULL;

static gboolean
button_press_event (GtkTreeView *                  view,
                    GdkEventButton *               ev,
                    UndertowLocationChooser *      chooser)
{
    GtkTreePath *tree_path;

    chooser->dnd_x = ev->x;
    chooser->dnd_y = ev->y;
    if (ev->button != 3)
        return FALSE;
    if (gtk_tree_view_get_path_at_pos (GTK_TREE_VIEW (view),
                                       ev->x, ev->y,
                                       &tree_path,
                                       NULL, NULL, NULL)) {
        gtk_tree_path_free (tree_path);
        /*
        g_signal_emit (chooser,
                       signals[POPUP_CONTEXT_MENU_SIGNAL],
                       0,
                       (gint) ev->x,
                       (gint) ev->y,
                       (guint) ev->button,
                       (guint) ev->time);
                       */
        g_debug ("popup-context-menu");
    }
    return FALSE;
}

static GtkTargetEntry dnd_targets[] =
{
    { "UndertowPlaceEntry", GTK_TARGET_SAME_WIDGET, 1 }
};

/*
 * step 1 of DnD.  the drag source sets the drag icon to the tree view row pixmap.
 */
static void
drag_begin (GtkTreeView *                  view,
            GdkDragContext *               context,
            UndertowLocationChooser *      chooser)
{
    GtkTreePath *path;
    GdkPixmap *icon;
    gint cell_y;

    if (!gtk_tree_view_get_path_at_pos (view, 
                                        chooser->dnd_x,
                                        chooser->dnd_y,
                                        &path,
                                        NULL, NULL, &cell_y))
        return;
    g_debug ("drag_begin");
    icon = gtk_tree_view_create_row_drag_icon (view, path);
    gtk_tree_path_free (path);
    gtk_drag_set_icon_pixmap (context,
                              gdk_drawable_get_colormap (icon),
                              icon,
                              NULL,
                              chooser->dnd_x + 1,
                              cell_y + 1);
    g_object_unref (icon);
 }

/*
 * step 2 of DnD.  The destination looks at the drop point to see whether
 * it is valid.
 */
static gboolean
drag_drop (GtkTreeView *                   view,
           GdkDragContext *                context,
           gint                            x,
           gint                            y,
           guint                           time,
           UndertowLocationChooser *       chooser)
{
    GtkTreePath *path;
    GtkTreeIter src_iter, dest_iter, *dest_iter_ptr;
    GtkTreeViewDropPosition position;
    GdkAtom target_type;

    if (!gtk_tree_view_get_dest_row_at_pos (view, x, y, &path, &position))
        dest_iter_ptr = NULL;
    else {
        gtk_tree_model_get_iter (chooser->model, &dest_iter, path);
        gtk_tree_path_free (path);
        dest_iter_ptr = &dest_iter;
    }
    target_type = GDK_POINTER_TO_ATOM (context->targets->data);
    gtk_drag_get_data (GTK_WIDGET (view), context, target_type, time);
    gtk_tree_model_get_iter_from_string (GTK_TREE_MODEL (chooser->model),
                                         &src_iter,
                                         chooser->dnd_src);
    undertow_location_model_move (UNDERTOW_LOCATION_MODEL (chooser->model),
                                  &src_iter,
                                  dest_iter_ptr,
                                  position);
    return TRUE;
}

/*
 * step 3 of DnD.  The source is receives a request for information about the
 * drag.  In our case we return the URL associated with the tree view row.
 */
static void
drag_data_get (GtkTreeView *               view,
               GdkDragContext *            context,
               GtkSelectionData *          selection,
               guint                       info,
               guint                       time,
               UndertowLocationChooser *   chooser)
{
    GtkTreePath *path;
    gchar *path_string;

    if (info != 1)
        g_error ("drag_data_get: info != 1");

    if (!gtk_tree_view_get_path_at_pos (view, 
                                        chooser->dnd_x,
                                        chooser->dnd_y,
                                        &path,
                                        NULL, NULL, NULL))
        return;
    path_string = gtk_tree_path_to_string (path);
    gtk_tree_path_free (path);
    g_debug ("drag_data_get: src row is %s", path_string);
    gtk_selection_data_set (selection,
                            selection->target,
                            sizeof (guchar),
                            (guchar *) path_string,
                            strlen (path_string));
    g_free (path_string);
}

/*
 * step 4 of DnD.  The destination receives the source info.  After parsing the
 * info the destination calls gtk_drag_finish, which indicates whether the drag
 * should succeed or not.
 */
static void
drag_data_recvd (GtkTreeView *             view,
                 GdkDragContext *          context,
                 gint                      x,
                 gint                      y,
                 GtkSelectionData *        selection,
                 guint                     info,
                 guint                     time,
                 UndertowLocationChooser * chooser)
{
    gchar *path_string = (gchar *) selection->data;
    g_debug ("drag_data_recvd: src row is %s", path_string);
    chooser->dnd_src = g_strdup (path_string);
    gtk_drag_finish (context, TRUE, FALSE, time);
}

static void
drag_end (GtkTreeView *                 view,
          GdkDragContext *              context,
          UndertowLocationChooser *     chooser)
{
    if (chooser->dnd_src)
        g_free (chooser->dnd_src);
    chooser->dnd_src = NULL;
    chooser->dnd_x = 0;
    chooser->dnd_y = 0;
}

static void
selection_changed (GtkTreeSelection *           selection,
                   UndertowLocationChooser *    chooser)
{
    GtkTreeIter iter;
    gchar *url;

    if (!gtk_tree_selection_get_selected (selection, NULL, &iter))
        return;
    gtk_tree_model_get (GTK_TREE_MODEL (chooser->model), &iter,
                        UNDERTOW_LOCATION_MODEL_URL_COLUMN, &url,
                        -1);
    g_signal_emit (chooser, signals[LOCATION_CHANGED_SIGNAL], 0, g_strdup (url));
    if (chooser->current_url)
        g_free (chooser->current_url);
    chooser->current_url = url;
}

static void
location_chooser_init (UndertowLocationChooser *            chooser,
                       UndertowLocationChooserClass *       chooser_class)
{
    GtkCellRenderer *title_renderer;
    GtkTreeViewColumn *title_column;

    /* create the location view */
    chooser->model = GTK_TREE_MODEL (undertow_location_model_get ());
    chooser->view = gtk_tree_view_new_with_model (chooser->model);
    gtk_tree_view_expand_all (GTK_TREE_VIEW (chooser->view));
    gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (chooser->view), FALSE);
    gtk_tree_view_enable_model_drag_source (GTK_TREE_VIEW (chooser->view),
                                            GDK_MODIFIER_MASK,
                                            dnd_targets, G_N_ELEMENTS (dnd_targets),
                                            GDK_ACTION_MOVE);
    gtk_tree_view_enable_model_drag_dest (GTK_TREE_VIEW (chooser->view),
                                          dnd_targets, G_N_ELEMENTS (dnd_targets),
                                          GDK_ACTION_MOVE);
    g_signal_connect (chooser->view, "button-press-event",
                      G_CALLBACK (button_press_event), chooser);
    g_signal_connect (chooser->view, "drag-begin",
                      G_CALLBACK (drag_begin), chooser);
    g_signal_connect (chooser->view, "drag-data-get",
                      G_CALLBACK (drag_data_get), chooser);
    g_signal_connect (chooser->view, "drag-data-received",
                      G_CALLBACK (drag_data_recvd), chooser);
    g_signal_connect (chooser->view, "drag-drop",
                      G_CALLBACK (drag_drop), chooser);
    g_signal_connect (chooser->view, "drag-end",
                      G_CALLBACK (drag_end), chooser);
    gtk_box_pack_start (GTK_BOX (chooser), chooser->view, TRUE, TRUE, 0);

    title_renderer = gtk_cell_renderer_text_new ();
    title_column =
        gtk_tree_view_column_new_with_attributes ("Title",
                                                  title_renderer,
                                                  "markup", UNDERTOW_LOCATION_MODEL_TITLE_COLUMN,
                                                  NULL);
    gtk_tree_view_append_column (GTK_TREE_VIEW (chooser->view), title_column);

    chooser->selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (chooser->view));
    gtk_tree_selection_set_mode (chooser->selection, GTK_SELECTION_SINGLE);
    g_signal_connect (chooser->selection, "changed", G_CALLBACK (selection_changed), chooser);
}

static void
location_chooser_dispose (UndertowLocationChooser *chooser)
{
    if (chooser->disposed)
        return;
    g_debug ("location_chooser_dispose");
    G_OBJECT_CLASS (parent_class)->dispose (G_OBJECT (chooser));
    g_object_unref (chooser->model);
    if (chooser->current_url)
        g_free (chooser->current_url);
    if (chooser->dnd_src)
        g_free (chooser->dnd_src);
    chooser->disposed = TRUE;
}

static void
location_chooser_class_init (UndertowLocationChooserClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    parent_class = g_type_class_peek_parent (klass);
    object_class->dispose = (GObjectFinalizeFunc) location_chooser_dispose;

    signals[LOCATION_CHANGED_SIGNAL] =
      g_signal_new ("location-changed",
                    G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_FIRST|G_SIGNAL_NO_RECURSE|G_SIGNAL_NO_HOOKS,
                    G_STRUCT_OFFSET (UndertowLocationChooserClass, location_changed),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__STRING,
                    G_TYPE_NONE, 1,
                    G_TYPE_STRING);
    /*
    signals[POPUP_CONTEXT_MENU_SIGNAL] =
      g_signal_new ("popup-context-menu",
                    G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST|G_SIGNAL_NO_RECURSE|G_SIGNAL_NO_HOOKS,
                    G_STRUCT_OFFSET (UndertowLocationChooserClass, popup_context_menu),
                    NULL, NULL,
                    undertow_cclosure_VOID__INT_INT_UINT_UINT,
                    G_TYPE_NONE, 4,
                    G_TYPE_INT, G_TYPE_INT, G_TYPE_UINT, G_TYPE_UINT);
                    */
}

GType 
undertow_location_chooser_get_type (void)
{
    static GType type = 0;

    if (!type) {
        static const GTypeInfo undertow_location_chooser_info = {
            sizeof (UndertowLocationChooserClass),
            NULL,
            NULL,
            (GClassInitFunc) location_chooser_class_init,
            NULL,
            NULL,
            sizeof (UndertowLocationChooser),
            0,
            (GInstanceInitFunc) location_chooser_init,
            NULL
        };
        type = g_type_register_static(GTK_TYPE_VBOX, 
                                      "UndertowLocationChooser",
                                      &undertow_location_chooser_info,
                                      0);
    }
    return type;
}

GtkWidget *
undertow_location_chooser_new (void)
{
    return GTK_WIDGET (g_object_new (UNDERTOW_TYPE_LOCATION_CHOOSER, NULL));
}

void
undertow_location_chooser_edit_current (UndertowLocationChooser *chooser)
{
    g_debug ("edit current location");
}

void
undertow_location_chooser_refresh_current (UndertowLocationChooser *chooser)
{
    g_debug ("refresh current location");
}

void
undertow_location_chooser_remove_current (UndertowLocationChooser *chooser)
{
    g_debug ("remove current location");
    /*
    GtkTreePath *tree_path;
    GtkTreeIter iter;
    UndertowChannel *channel;
    gchar *path;

    tree_path = gtk_tree_row_reference_get_path (rowref);
    gtk_tree_model_get_iter (GTK_TREE_MODEL (model), &iter, tree_path);
    gtk_tree_path_free (tree_path);
    gtk_tree_model_get (GTK_TREE_MODEL (model), &iter,
                        UNDERTOW_LOCATION_MODEL_PATH_COLUMN, &path,
                        -1);
    g_remove (path);
    gtk_tree_store_remove (GTK_TREE_STORE (model), &iter);
    g_free (path);
    undertow_channel_remove (channel);
    */
}
