#ifndef UNDERTOW_LOCATION_CHOOSER_H
#define UNDERTOW_LOCATION_CHOOSER_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

#include "undertow-location-model.h"

#define UNDERTOW_TYPE_LOCATION_CHOOSER             (undertow_location_chooser_get_type())
#define UNDERTOW_LOCATION_CHOOSER(obj)             \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), UNDERTOW_TYPE_LOCATION_CHOOSER, UndertowLocationChooser))
#define UNDERTOW_LOCATION_CHOOSER_CLASS(klass)     \
    (G_TYPE_CHECK_CLASS_CAST((klass), UNDERTOW_TYPE_LOCATION_CHOOSER, UndertowLocationChooserClass))
#define UNDERTOW_IS_LOCATION_CHOOSER(obj)          \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), UNDERTOW_TYPE_LOCATION_CHOOSER))
#define UNDERTOW_IS_LOCATION_CHOOSER_CLASS(klass)  \
    (G_TYPE_CHECK_CLASS_TYPE((klass), UNDERTOW_TYPE_LOCATION_CHOOSER))
#define UNDERTOW_LOCATION_CHOOSER_GET_CLASS(obj)   \
    (G_TYPE_INSTANCE_GET_CLASS((obj), UNDERTOW_TYPE_LOCATION_CHOOSER, UndertowLocationChooserClass))

typedef struct {
    GtkVBox parent;
    gboolean disposed;
    gchar *current_url;
    GtkTreeModel *model;
    GtkWidget *view;
    GtkTreeSelection *selection;
    gint dnd_x;
    gint dnd_y;
    gchar *dnd_src;
} UndertowLocationChooser;

typedef struct {
    GtkVBoxClass parent_class;
    void (*location_changed)(UndertowLocationChooser *, gchar *);
} UndertowLocationChooserClass;

GType
undertow_location_chooser_get_type (void);

GtkWidget *
undertow_location_chooser_new (void);

void
undertow_location_chooser_edit_current (UndertowLocationChooser *chooser);

void
undertow_location_chooser_refresh_current (UndertowLocationChooser *chooser);

void
undertow_location_chooser_remove_current (UndertowLocationChooser *chooser);

#endif
