#ifndef UNDERTOW_FIRST_RUN_H
#define UNDERTOW_FIRST_RUN_H

#include <glib.h>
#include <gconf/gconf-client.h>

gchar *undertow_first_run_check (GConfClient *gconf);

#endif
