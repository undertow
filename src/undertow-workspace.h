#ifndef UNDERTOW_WORKSPACE_H
#define UNDERTOW_WORKSPACE_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

#include "undertow-location-model.h"
#include "undertow-channel.h"

#define UNDERTOW_TYPE_WORKSPACE             (undertow_workspace_get_type())
#define UNDERTOW_WORKSPACE(obj)             \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), UNDERTOW_TYPE_WORKSPACE, UndertowWorkspace))
#define UNDERTOW_WORKSPACE_CLASS(klass)     \
    (G_TYPE_CHECK_CLASS_CAST((klass), UNDERTOW_TYPE_WORKSPACE, UndertowWorkspaceClass))
#define UNDERTOW_IS_WORKSPACE(obj)          \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), UNDERTOW_TYPE_WORKSPACE))
#define UNDERTOW_IS_WORKSPACE_CLASS(klass)  \
    (G_TYPE_CHECK_CLASS_TYPE((klass), UNDERTOW_TYPE_WORKSPACE))
#define UNDERTOW_WORKSPACE_GET_CLASS(obj)   \
    (G_TYPE_INSTANCE_GET_CLASS((obj), UNDERTOW_TYPE_WORKSPACE, UndertowWorkspaceClass))

typedef struct {
    GtkHPaned parent;
    gboolean disposed;
    GtkWidget *provider_chooser;
    GtkWidget *location_chooser;
    GtkWidget *switcher;
    UndertowLocationModel *locations;
    UndertowChannelManager *channels;
} UndertowWorkspace;

typedef struct {
    GtkHPanedClass parent_class;
} UndertowWorkspaceClass;

GType undertow_workspace_get_type (void);

GtkWidget *undertow_workspace_new (void);

void undertow_workspace_add_channel (UndertowWorkspace *        workspace,
                                     const gchar *              url,
                                     const gchar *              parent);
void undertow_workspace_add_folder (UndertowWorkspace *         workspace,
                                    const gchar *               name,
                                    const gchar *               parent);

void undertow_workspace_remove (UndertowWorkspace *             workspace,
                                const gchar *                   url);
void undertow_workspace_remove_current (UndertowWorkspace *     workspace);

void undertow_workspace_refresh (UndertowWorkspace *            workspace,
                                 const gchar *                  url);
void undertow_workspace_refresh_current (UndertowWorkspace *    workspace);

#endif
