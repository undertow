#ifndef UNDERTOW_CHANNEL_MANAGER_H
#define UNDERTOW_CHANNEL_MANAGER_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <time.h>
#include <unistd.h>

#include "undertow-channel.h"

#define UNDERTOW_TYPE_CHANNEL_MANAGER             (undertow_channel_manager_get_type())
#define UNDERTOW_CHANNEL_MANAGER(obj)             \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), UNDERTOW_TYPE_CHANNEL_MANAGER, UndertowChannelManager))
#define UNDERTOW_CHANNEL_MANAGER_CLASS(klass)     \
    (G_TYPE_CHECK_CLASS_CAST((klass), UNDERTOW_TYPE_CHANNEL_MANAGER, UndertowChannelManagerClass))
#define UNDERTOW_IS_CHANNEL_MANAGER(obj)          \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), UNDERTOW_TYPE_CHANNEL_MANAGER))
#define UNDERTOW_IS_CHANNEL_MANAGER_CLASS(klass)  \
    (G_TYPE_CHECK_CLASS_TYPE((klass), UNDERTOW_TYPE_CHANNEL_MANAGER))
#define UNDERTOW_CHANNEL_MANAGER_GET_CLASS(obj)   \
    (G_TYPE_INSTANCE_GET_CLASS((obj), UNDERTOW_TYPE_CHANNEL_MANAGER, UndertowChannelManagerClass))

typedef struct {
    GObject parent;
    GHashTable *channels;
    gboolean disposed;
} UndertowChannelManager;

typedef struct {
    GObjectClass parent_class;
    UndertowChannelManager *singleton;
    void (*channel_added)(UndertowChannelManager *, UndertowChannel *);
    void (*channel_removed)(UndertowChannelManager *, UndertowChannel *);
    void (*channel_error)(UndertowChannel *, const gchar *, GError *);
} UndertowChannelManagerClass;

GType undertow_channel_manager_get_type (void);

UndertowChannelManager *
undertow_channel_manager_get (void);

gboolean
undertow_channel_manager_add_channel (UndertowChannelManager *      manager,
                                      const gchar *                 url,
                                      GError **                     error);

UndertowChannel *
undertow_channel_manager_get_channel (UndertowChannelManager *      manager,
                                      const gchar *                 url);

void
undertow_channel_manager_refresh_channel (UndertowChannelManager *  manager,
                                          const gchar *             url);

typedef void (*UndertowChannelManagerForeachFunc)(UndertowChannelManager *, UndertowChannel *, gpointer);

void
undertow_channel_manager_foreach_channel (UndertowChannelManager *              manager,
                                          UndertowChannelManagerForeachFunc     func,
                                          gpointer                              userdata);

#endif
