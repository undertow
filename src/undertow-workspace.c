#include <gtk/gtk.h>
#include <string.h>

#include "undertow-workspace.h"
#include "undertow-location-chooser.h"
#include "undertow-channel.h"
#include "undertow-channel-page.h"
#include "undertow-workspace-switcher.h"

static GtkHPanedClass *parent_class = NULL;

static void
add_page (UndertowChannelManager *      manager,
          UndertowChannel *             channel,
          UndertowWorkspace *           workspace)
{
    UndertowWorkspaceSwitcher *switcher = UNDERTOW_WORKSPACE_SWITCHER (workspace->switcher);
    GtkWidget *page;
    gchar *channel_url;
   
    g_object_get (channel, "url", &channel_url, NULL);
    page = undertow_channel_page_new (channel);
    gtk_widget_show_all (page);
    undertow_workspace_switcher_add_page (switcher, channel_url, page);
    g_debug ("added page for %s in workspace switcher", channel_url);
    g_free (channel_url);
}

static gboolean
add_location (UndertowLocationModel *       locations,
              GtkTreePath *                 tree_path,
              GtkTreeIter *                 tree_iter,
              UndertowWorkspace *           workspace)
{
    UndertowChannel *channel;
    gchar *url;

    gtk_tree_model_get (GTK_TREE_MODEL (locations), tree_iter,
                        UNDERTOW_LOCATION_MODEL_URL_COLUMN, &url,
                        -1);
    if (!strncmp (url, "folder://", 9)) {
        undertow_workspace_switcher_add_page (
                UNDERTOW_WORKSPACE_SWITCHER (workspace->switcher),
                url,
                gtk_label_new (NULL));
        return FALSE;
    }
    if (!strncmp (url, "http://", 7) || !strncmp (url, "https://", 8)) {
        channel = undertow_channel_manager_get_channel (workspace->channels, url);
        if (channel == NULL) {
            g_critical ("failed to add location: %s is not in the channel manager", url);
            g_free (url);
            return FALSE;
        }
        add_page (workspace->channels, channel, workspace);
        return FALSE;
    }
    g_critical ("failed to add location: URL %s has an unknown scheme", url);
    return FALSE;
}

static void
location_changed (UndertowLocationChooser *     chooser,
                  gchar *                       url,
                  UndertowWorkspace *           workspace)
{
    undertow_workspace_switcher_set_page (UNDERTOW_WORKSPACE_SWITCHER (workspace->switcher), url);
}

static void
workspace_dispose (UndertowWorkspace *workspace)
{
    if (workspace->disposed)
        return;
    g_debug ("workspace_dispose");
    G_OBJECT_CLASS (parent_class)->dispose (G_OBJECT (workspace));
    g_object_unref (workspace->locations);
    g_object_unref (workspace->channels);
    workspace->disposed = TRUE;
}

static void
workspace_init (UndertowWorkspace *              workspace,
                UndertowWorkspaceClass *         klass)
{
    GtkWidget *align, *frame;

    /* chooser pane */
    align = gtk_alignment_new (0.5, 0.5, 1.0, 1.0);
    gtk_alignment_set_padding (GTK_ALIGNMENT (align), 6, 6, 3, 0);
    gtk_paned_add1 (GTK_PANED (workspace), align);
    frame = gtk_frame_new (NULL);
    gtk_container_add (GTK_CONTAINER (align), frame);
    workspace->locations = undertow_location_model_get ();
    workspace->location_chooser = undertow_location_chooser_new ();
    gtk_widget_set_size_request (GTK_WIDGET (workspace->location_chooser), 200, -1);
    g_signal_connect (workspace->location_chooser,
                      "location-changed",
                      G_CALLBACK (location_changed),
                      workspace);
    gtk_container_add (GTK_CONTAINER (frame), workspace->location_chooser);

    /* workspace pane */
    align = gtk_alignment_new (0.5, 0.5, 1.0, 1.0);
    gtk_alignment_set_padding (GTK_ALIGNMENT (align), 6, 6, 0, 3);
    gtk_paned_add2 (GTK_PANED (workspace), align);
    frame = gtk_frame_new (NULL);
    gtk_container_add (GTK_CONTAINER (align), frame);
    workspace->switcher = undertow_workspace_switcher_new ();
    gtk_container_add (GTK_CONTAINER (frame), workspace->switcher);
    workspace->channels = undertow_channel_manager_get ();
    g_signal_connect (workspace->channels, "channel-added", G_CALLBACK (add_page), workspace);
    gtk_tree_model_foreach (GTK_TREE_MODEL (workspace->locations),
                            (GtkTreeModelForeachFunc) add_location,
                            workspace);
}

static void
workspace_class_init (UndertowWorkspaceClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    parent_class = g_type_class_peek_parent (klass);

    object_class->dispose = (GObjectFinalizeFunc) workspace_dispose;
}

GType 
undertow_workspace_get_type (void)
{
    static GType type = 0;

    if (!type) {
        static const GTypeInfo info = {
            sizeof (UndertowWorkspaceClass),
            NULL,
            NULL,
            (GClassInitFunc) workspace_class_init,
            NULL,
            NULL,
            sizeof (UndertowWorkspace),
            0,
            (GInstanceInitFunc) workspace_init,
            NULL
        };
        type = g_type_register_static (GTK_TYPE_HPANED, 
                                       "UndertowWorkspace",
                                       &info, 0);
    }
    return type;
}

GtkWidget *
undertow_workspace_new (void)
{
    return GTK_WIDGET (g_object_new (UNDERTOW_TYPE_WORKSPACE, NULL));
}

void
undertow_workspace_add_channel (UndertowWorkspace *         workspace,
                                const gchar *               url,
                                const gchar *               parent)
{
    undertow_location_model_add_channel (workspace->locations, url, parent);
}

void
undertow_workspace_add_folder (UndertowWorkspace *          workspace,
                               const gchar *                name,
                               const gchar *                parent)
{
    undertow_location_model_add_folder (workspace->locations, name, parent);
}

void
undertow_workspace_remove (UndertowWorkspace *workspace, const gchar *url)
{
    gchar *current;

    g_object_get (workspace->location_chooser, "current", &current, NULL);
    if (current != NULL && !strcmp (current, url))
        g_object_set (workspace->location_chooser, "current", NULL, NULL);
    if (current != NULL)
        g_free (current);
    undertow_location_model_remove (workspace->locations, url);
}

void
undertow_workspace_remove_current (UndertowWorkspace *workspace)
{
    gchar *url;

    g_object_get (workspace->location_chooser, "current", &url, NULL);
    if (url == NULL)
        return;
    undertow_workspace_remove (workspace, url);
    /* undertow_workspace_switcher_remove_page (UNDERTOW_WORKSPACE_SWITCHER (workspace->switcher), url); */
    g_free (url);
}

void
undertow_workspace_refresh (UndertowWorkspace *workspace, const gchar *url)
{
    undertow_channel_manager_refresh_channel (workspace->channels, url);
}
   
void
undertow_workspace_refresh_current (UndertowWorkspace *workspace)
{
    gchar *url;

    g_object_get (workspace->location_chooser, "current", &url, NULL);
    if (url == NULL)
        return;
    undertow_workspace_refresh (workspace, url);
    g_free (url);
}
