#ifndef UNDERTOW_FOLDER_BROWSER_H
#define UNDERTOW_FOLDER_BROWSER_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

#define UNDERTOW_TYPE_FOLDER_BROWSER             (undertow_folder_browser_get_type())
#define UNDERTOW_FOLDER_BROWSER(obj)             \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), UNDERTOW_TYPE_FOLDER_BROWSER, UndertowFolderBrowser))
#define UNDERTOW_FOLDER_BROWSER_CLASS(klass)     \
    (G_TYPE_CHECK_CLASS_CAST((klass), UNDERTOW_TYPE_FOLDER_BROWSER, UndertowFolderBrowserClass))
#define UNDERTOW_IS_FOLDER_BROWSER(obj)          \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), UNDERTOW_TYPE_FOLDER_BROWSER))
#define UNDERTOW_IS_FOLDER_BROWSER_CLASS(klass)  \
    (G_TYPE_CHECK_CLASS_TYPE((klass), UNDERTOW_TYPE_FOLDER_BROWSER))
#define UNDERTOW_FOLDER_BROWSER_GET_CLASS(obj)   \
    (G_TYPE_INSTANCE_GET_CLASS((obj), UNDERTOW_TYPE_FOLDER_BROWSER, UndertowFolderBrowserClass))

typedef struct {
    GtkScrolledWindow parent;
    gboolean disposed;
    GtkTreeStore *store;
    GtkWidget *view;
    GtkTreeSelection *selection;
} UndertowFolderBrowser;

typedef struct {
    GtkScrolledWindowClass parent_class;
} UndertowFolderBrowserClass;

GType undertow_folder_browser_get_type (void);

GtkWidget *undertow_folder_browser_new (void);
gchar *undertow_folder_browser_get_selected (UndertowFolderBrowser *browser);

#endif
