#ifndef UNDERTOW_WORKSPACE_SWITCHER_H
#define UNDERTOW_WORKSPACE_SWITCHER_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

#define UNDERTOW_TYPE_WORKSPACE_SWITCHER             (undertow_workspace_switcher_get_type())
#define UNDERTOW_WORKSPACE_SWITCHER(obj)             \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), UNDERTOW_TYPE_WORKSPACE_SWITCHER, UndertowWorkspaceSwitcher))
#define UNDERTOW_WORKSPACE_SWITCHER_CLASS(klass)     \
    (G_TYPE_CHECK_CLASS_CAST((klass), UNDERTOW_TYPE_WORKSPACE_SWITCHER, UndertowWorkspaceSwitcherClass))
#define UNDERTOW_IS_WORKSPACE_SWITCHER(obj)          \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), UNDERTOW_TYPE_WORKSPACE_SWITCHER))
#define UNDERTOW_IS_WORKSPACE_SWITCHER_CLASS(klass)  \
    (G_TYPE_CHECK_CLASS_TYPE((klass), UNDERTOW_TYPE_WORKSPACE_SWITCHER))
#define UNDERTOW_WORKSPACE_SWITCHER_GET_CLASS(obj)   \
    (G_TYPE_INSTANCE_GET_CLASS((obj), UNDERTOW_TYPE_WORKSPACE_SWITCHER, UndertowWorkspaceSwitcherClass))

typedef struct {
    GtkContainer parent;
    gboolean disposed;
    GHashTable *pages;
    GtkWidget *current;
    GtkWidget *blank;
} UndertowWorkspaceSwitcher;

typedef struct {
    GtkContainerClass parent_class;
} UndertowWorkspaceSwitcherClass;

GType
undertow_workspace_switcher_get_type (void);

GtkWidget *
undertow_workspace_switcher_new (void);

void
undertow_workspace_switcher_set_page (UndertowWorkspaceSwitcher *       switcher,
                                      const gchar *                     url);

void
undertow_workspace_switcher_add_page (UndertowWorkspaceSwitcher *       switcher,
                                      const gchar *                     url,
                                      GtkWidget *                       page);

void
undertow_workspace_switcher_remove_page (UndertowWorkspaceSwitcher *    switcher,
                                         const gchar *                  url);

#endif
