#ifndef UNDERTOW_LOCATION_MODEL_H
#define UNDERTOW_LOCATION_MODEL_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

#include "undertow-channel-manager.h"

#define UNDERTOW_TYPE_LOCATION_MODEL             (undertow_location_model_get_type())
#define UNDERTOW_LOCATION_MODEL(obj)             \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), UNDERTOW_TYPE_LOCATION_MODEL, UndertowLocationModel))
#define UNDERTOW_LOCATION_MODEL_CLASS(klass)     \
    (G_TYPE_CHECK_CLASS_CAST((klass), UNDERTOW_TYPE_LOCATION_MODEL, UndertowLocationModelClass))
#define UNDERTOW_IS_LOCATION_MODEL(obj)          \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), UNDERTOW_TYPE_LOCATION_MODEL))
#define UNDERTOW_IS_LOCATION_MODEL_CLASS(klass)  \
    (G_TYPE_CHECK_CLASS_TYPE((klass), UNDERTOW_TYPE_LOCATION_MODEL))
#define UNDERTOW_LOCATION_MODEL_GET_CLASS(obj)   \
    (G_TYPE_INSTANCE_GET_CLASS((obj), UNDERTOW_TYPE_LOCATION_MODEL, UndertowLocationModelClass))

enum {
    UNDERTOW_LOCATION_MODEL_TYPE_COLUMN,
    UNDERTOW_LOCATION_MODEL_URL_COLUMN,
    UNDERTOW_LOCATION_MODEL_TITLE_COLUMN,
    UNDERTOW_LOCATION_MODEL_PATH_COLUMN,
    UNDERTOW_LOCATION_MODEL_N_COLUMNS
};

typedef enum {
    UNDERTOW_LOCATION_TYPE_CHANNEL,
    UNDERTOW_LOCATION_TYPE_FOLDER,
    UNDERTOW_LOCATION_TYPE_PLAYLIST
} UndertowLocationType;

typedef struct {
    GtkTreeStore parent;
    UndertowChannelManager *channels;
    gboolean disposed;
} UndertowLocationModel;

typedef struct {
    GtkTreeStoreClass parent_class;
    GObject *singleton;
    void (*location_added)(UndertowLocationModel *);
    void (*location_removed)(UndertowLocationModel *);
} UndertowLocationModelClass;

GType undertow_location_model_get_type (void);

UndertowLocationModel *undertow_location_model_get (void);

void 
undertow_location_model_add_channel (UndertowLocationModel *model);

void
undertow_location_model_add_folder (UndertowLocationModel *model);

void
undertow_location_model_add_playlist (UndertowLocationModel *model);

void
undertow_location_model_move (UndertowLocationModel *model,
                              GtkTreeIter *src_iter,
                              GtkTreeIter *dest_iter,
                              GtkTreeViewDropPosition position);

#endif
