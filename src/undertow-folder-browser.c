#include <string.h>
#include <gtk/gtk.h>
#include <glib.h>

#include "undertow-folder-browser.h"
#include "undertow-location-model.h"

enum {
    LOCATION_URL_COLUMN,
    LOCATION_TITLE_COLUMN,
    N_LOCATION_COLUMNS
};

static GtkScrolledWindowClass *parent_class = NULL;

/************************/
/* FolderBrowser widget */
/************************/

static GObjectClass *folder_browser_parent_class = NULL;

static void
load_channel_folders (GtkTreeStore *    dst_store,
                      GtkTreeModel *    src_model,
                      GtkTreeIter *     src,
                      GtkTreeIter *     dst_parent)
{
    GtkTreeIter dst, src_child;
    gchar *url, *title;

    do {
        gtk_tree_model_get (src_model, src,
                            UNDERTOW_LOCATION_MODEL_URL_COLUMN, &url,
                            UNDERTOW_LOCATION_MODEL_TITLE_COLUMN, &title,
                            -1);
        if (!strncmp (url, "folder://", 9)) {
            gtk_tree_store_append (dst_store, &dst, dst_parent);
            gtk_tree_store_set (dst_store, &dst,
                                UNDERTOW_LOCATION_MODEL_URL_COLUMN, url,
                                UNDERTOW_LOCATION_MODEL_TITLE_COLUMN, title,
                                -1);
            if (gtk_tree_model_iter_children (src_model, &src_child, src))
                load_channel_folders (dst_store, src_model, &src_child, &dst);
        }
        g_free (url);
        g_free (title);
    }
    while (gtk_tree_model_iter_next (src_model, src));
}

static void
folder_browser_init (UndertowFolderBrowser *browser)
{
    UndertowLocationModel *locations;
    GtkCellRenderer *folder_title;
    GtkTreeViewColumn *folder_title_column;
    GtkTreeIter iter, src;

    /* create the folder view */
    browser->store = gtk_tree_store_new (N_LOCATION_COLUMNS,
                                         G_TYPE_STRING,
                                         G_TYPE_STRING);
    browser->view = gtk_tree_view_new_with_model (GTK_TREE_MODEL (browser->store));
    gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (browser->view), FALSE);
    gtk_tree_view_set_reorderable (GTK_TREE_VIEW (browser->view), FALSE);
    gtk_container_add (GTK_CONTAINER (browser), browser->view);

    folder_title = gtk_cell_renderer_text_new ();
    folder_title_column =
        gtk_tree_view_column_new_with_attributes ("Title",
                                                  folder_title,
                                                  "markup", LOCATION_TITLE_COLUMN,
                                                  NULL);
    gtk_tree_view_append_column (GTK_TREE_VIEW (browser->view), folder_title_column);

    browser->selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (browser->view));
    gtk_tree_selection_set_mode (browser->selection, GTK_SELECTION_SINGLE);

    /* populate the folder model */
    locations = undertow_location_model_get ();
    gtk_tree_store_append (GTK_TREE_STORE (browser->store), &iter, NULL);
    gtk_tree_store_set (browser->store, &iter,
                        LOCATION_URL_COLUMN, "folder://",
                        LOCATION_TITLE_COLUMN, "<b>Channels</b>",
                        -1);
    if (gtk_tree_model_get_iter_first (GTK_TREE_MODEL (locations), &src))
        load_channel_folders (browser->store,
                              GTK_TREE_MODEL (locations),
                              &src, &iter);
    gtk_tree_view_expand_all (GTK_TREE_VIEW (browser->view));
    g_object_unref (locations);
}

static void
folder_browser_get_property (UndertowFolderBrowser *    browser,
                             guint                      property_id,
                             GValue *                   value,
                             GParamSpec *               spec)
{
    switch (property_id) {
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (G_OBJECT (browser),
                                               property_id,
                                               spec);
            break;
    }
}

static void 
folder_browser_set_property (UndertowFolderBrowser *    browser,
                             guint                      property_id,
                             const GValue *             value,
                             GParamSpec *               spec)
{
    switch (property_id) {
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (G_OBJECT (browser),
                                               property_id,
                                               spec);
            break;
    }
}

static void
folder_browser_dispose (UndertowFolderBrowser *browser)
{
    if (browser->disposed)
        return;
    g_debug ("folder_browser_dispose");
    G_OBJECT_CLASS (parent_class)->dispose (G_OBJECT (browser));
    browser->disposed = TRUE;
}

static void
folder_browser_class_init (UndertowFolderBrowserClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);

    parent_class = g_type_class_peek_parent (klass);

    folder_browser_parent_class = g_type_class_peek_parent (klass);
    object_class->get_property =
        (GObjectGetPropertyFunc) folder_browser_get_property;
    object_class->set_property =
        (GObjectSetPropertyFunc) folder_browser_set_property;
    object_class->dispose =
        (GObjectFinalizeFunc) folder_browser_dispose;
}

GType 
undertow_folder_browser_get_type (void)
{
    static GType type = 0;

    if (!type) {
        static const GTypeInfo undertow_folder_browser_info = {
            sizeof (UndertowFolderBrowserClass),
            NULL,
            NULL,
            (GClassInitFunc) folder_browser_class_init,
            NULL,
            NULL,
            sizeof (UndertowFolderBrowser),
            0,
            (GInstanceInitFunc) folder_browser_init,
            NULL
        };
        type = g_type_register_static(GTK_TYPE_VBOX, 
                                      "UndertowFolderBrowser",
                                      &undertow_folder_browser_info,
                                      0);
    }
    return type;
}

GtkWidget *
undertow_folder_browser_new (void)
{
    return GTK_WIDGET (g_object_new (UNDERTOW_TYPE_FOLDER_BROWSER, NULL));
}

gchar *
undertow_folder_browser_get_selected (UndertowFolderBrowser *browser)
{
    GtkTreeIter iter;
    gchar *url;

    if (!gtk_tree_selection_get_selected (browser->selection, NULL, &iter))
        return g_strdup ("folder://");
    gtk_tree_model_get (GTK_TREE_MODEL (browser->store), &iter,
                        LOCATION_URL_COLUMN, &url,
                        -1);
    return url;
}
