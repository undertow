#include <gtk/gtk.h>
#include <glib.h>
#include <glib/gstdio.h>
#include <gconf/gconf-client.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

#include "undertow-first-run.h"

static void
display_error_and_exit (const gchar *message, const gchar *reason)
{
    GtkWidget *dialog;

    dialog = gtk_message_dialog_new (NULL,
                                     0,
                                     GTK_MESSAGE_ERROR,
                                     GTK_BUTTONS_OK,
                                     message);
    gtk_message_dialog_format_secondary_text (GTK_MESSAGE_DIALOG (dialog), reason);
    gtk_widget_show_all (dialog);
    gtk_dialog_run (GTK_DIALOG (dialog));
    gtk_widget_destroy (dialog);
    exit (1);
}

gchar *
undertow_first_run_check (GConfClient *gconf)
{
    gchar *data_dir, *path;
    GError *error = NULL;

    data_dir = gconf_client_get_string (gconf, "/apps/undertow/data-dir", &error);
    if (error)
        display_error_and_exit ("Failed to read settings", error->message);

    if (data_dir == NULL) {
        data_dir = g_build_path ("/", g_get_home_dir (), ".undertow", NULL);
        gconf_client_set_string (gconf, "/apps/undertow/data-dir", data_dir, NULL);
    }
    if (g_access (data_dir, F_OK) != 0) {
        if (g_mkdir (data_dir, 0755) < 0)
            display_error_and_exit ("Failed to create application directory", strerror (errno));
    }

    path = g_build_path ("/", data_dir, "channels", NULL);
    if (g_access (path, F_OK) != 0) {
        if (g_mkdir (path, 0755) < 0)
            display_error_and_exit ("Failed to create channels directory", strerror (errno));
    }
    g_free (path);

    path = g_build_path ("/", data_dir, "videos", NULL);
    if (g_access (path, R_OK) != 0) {
        if (g_mkdir (path, 0755) < 0)
            display_error_and_exit ("Failed to create videos directory", strerror (errno));
    }
    g_free (path);

    return data_dir;
}
