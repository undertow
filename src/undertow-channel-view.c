#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <goocanvas.h>
#include <libundertow.h>

#include "undertow-channel-view.h"
#include "undertow-episode.h"
#include "undertow-reflow-label.h"

typedef struct {
    UndertowChannelView *view;
    GdkRectangle rect;
    GooCanvasItem *group;
    GooCanvasItem *separator;
    GooCanvasItem *control;
    GtkWidget *box;
    GtkWidget *text;
    GtkWidget *status;
    GtkWidget *button;
    UndertowEpisode *episode;
    time_t published;
} ViewItem;

enum {
    CHANNEL_VIEW_MODEL = 1,
};

static GooCanvasClass *parent_class = NULL;

/*
 * callbacks
 */
static void
on_episode_added (UndertowChannel *         channel,
                  UndertowEpisode *         episode,
                  UndertowChannelView *     view)
{
    g_debug ("added episode");
}

static void
on_episode_expired (UndertowChannel *       channel,
                    UndertowEpisode *       episode,
                    UndertowChannelView *   view)
{
    g_debug ("episode expired");
}

static void
on_state_changed (UndertowEpisode *         episode,
                  UndertowDownloadState     state,
                  ViewItem *                vitem)
{
    switch (state) {
        case UNDERTOW_DOWNLOAD_STATE_NOT_RUNNING:
            gtk_button_set_label (GTK_BUTTON (vitem->button), "Download");
            g_debug ("state changed (not running)");
            break;
        case UNDERTOW_DOWNLOAD_STATE_RUNNING:
            gtk_button_set_label (GTK_BUTTON (vitem->button), "Pause");
            g_debug ("state changed (running)");
            break;
        case UNDERTOW_DOWNLOAD_STATE_FINISHED:
            gtk_button_set_label (GTK_BUTTON (vitem->button), "Watch");
            g_debug ("state changed (complete)");
            break;
    }
}

static void
on_dl_progress (UndertowEpisode *           episode,
                guint                       length,
                guint                       transferred,
                gfloat                      fraction,
                gfloat                      speed,
                ViewItem *                  vitem)
{
    gchar *status;

    status = g_strdup_printf ("Downloading... %.1f%% (%.1f k/s)", fraction * 100.0, speed/1000.0);
    gtk_label_set_text (GTK_LABEL (vitem->status), status);
    g_free (status);
}

static void
on_dl_error (UndertowEpisode *      episode,
             GError *               error,
             ViewItem *             vitem)
{
    gchar *status;

    status = g_strdup_printf ("Failed to Download (%s)", error->message);
    gtk_label_set_text (GTK_LABEL (vitem->status), status);
    g_free (status);
}

static gboolean
on_click (GtkWidget *button, ViewItem *vitem)
{
    gchar *content_url;
    GError *error = NULL;

    g_object_get (vitem->episode, "content-url", &content_url, NULL);
    switch (undertow_episode_get_state (vitem->episode)) {
        case UNDERTOW_DOWNLOAD_STATE_NOT_RUNNING:
            undertow_episode_start_download (vitem->episode, &error);
            if (error != NULL) {
                g_critical ("failed to start downloading %s: %s",
                            content_url,
                            error->message);
                g_error_free (error);
            }
            break;
        case UNDERTOW_DOWNLOAD_STATE_RUNNING:
            g_debug ("pause download of %s", content_url);
            break;
        case UNDERTOW_DOWNLOAD_STATE_FINISHED:
            g_debug ("watch episode");
            break;
    }
    g_free (content_url);
    return TRUE;
}

/*
 *
 */
static void
redraw_canvas_region (GooCanvas *canvas, GdkRectangle *redraw_region)
{
    GooCanvasBounds bounds;

    bounds.x1 = (gdouble) redraw_region->x;
    bounds.y1 = (gdouble) redraw_region->y;
    bounds.x2 = (gdouble) (redraw_region->x + redraw_region->width);
    bounds.y2 = (gdouble) (redraw_region->y + redraw_region->height);
    goo_canvas_request_redraw (canvas, &bounds);
}

/*
 *
 */
static void
insert_view_item (UndertowChannelView *view, UndertowEpisode *episode)
{
    ViewItem *vitem;
    GList *list_item;
    gchar *title, *desc, *output, *line_path;
    GtkWidget *hbox;
    GooCanvasBounds bounds;
    GdkRectangle redraw_region;

    /* create the new ViewItem */
    vitem = g_new0 (ViewItem, 1);
    vitem->episode = episode;
    g_object_get (episode,
                  "title", &title,
                  "description", &desc,
                  "published", &vitem->published,
                  NULL);

    /* find where to insert the new episode */
    list_item = view->items;
    while (1) {
        if (list_item != NULL) {
            ViewItem *vitem2 = (ViewItem *) list_item->data;
            if (vitem->published > vitem2->published) {
                view->items = g_list_insert_before (view->items, list_item, vitem);
                break;
            }
            list_item = g_list_next (list_item);
        }
        else {
            view->items = g_list_append (view->items, vitem);
            break;
        }
    }

    /* set the item's clipping rectangle.  we don't know the item's height
     * until we have created the text portion of the item, so it is set later.
     */
    vitem->rect.x = 0;
    vitem->rect.y = view->items_height;
    vitem->rect.width = view->canvas_width;
    vitem->rect.height = 12;

    /* connect to the necessary signals so we know when to update the ViewItem */
    g_signal_connect (episode, "state-changed", G_CALLBACK (on_state_changed), vitem);
    g_signal_connect (episode, "download-progress", G_CALLBACK (on_dl_progress), vitem);
    g_signal_connect (episode, "download-error", G_CALLBACK (on_dl_error), vitem);

    /* each canvas item for the view item will be a child of vitem->group */
    vitem->group = goo_canvas_group_new (view->root_item, NULL);

    /* create the episode text */
    vitem->box = gtk_vbox_new (FALSE, 0);
    output = g_markup_printf_escaped (
                "<span size='medium' weight='bold'>%s</span>\n\n<span size='medium'>%s</span>",
                title, desc);
    g_free (title);
    g_free (desc);
    vitem->text = undertow_reflow_label_new_with_markup (output);
    g_free (output);
    gtk_box_pack_start (GTK_BOX (vitem->box), vitem->text, TRUE, TRUE, 9);

    /* create the episode download controls */
    hbox = gtk_hbox_new (FALSE, 0);
    switch (undertow_episode_get_state (vitem->episode)) {
        case UNDERTOW_DOWNLOAD_STATE_NOT_RUNNING:
            vitem->button = gtk_button_new_with_label ("Download");
            break;
        case UNDERTOW_DOWNLOAD_STATE_RUNNING:
            vitem->button = gtk_button_new_with_label ("Pause");
            break;
        case UNDERTOW_DOWNLOAD_STATE_FINISHED:
            vitem->button = gtk_button_new_with_label ("Watch");
            break;
    }
    gtk_box_pack_start (GTK_BOX (hbox), vitem->button, FALSE, FALSE, 6);
    g_signal_connect (vitem->button, "clicked", G_CALLBACK (on_click), vitem);
    vitem->status = gtk_label_new (NULL);
    gtk_label_set_selectable (GTK_LABEL (vitem->status), FALSE);
    gtk_box_pack_start (GTK_BOX (hbox), vitem->status, FALSE, FALSE, 0);
    gtk_box_pack_start (GTK_BOX (vitem->box), hbox, FALSE, FALSE, 0);
    gtk_widget_show_all (vitem->box);
    vitem->control = goo_canvas_widget_new (vitem->group,
                                            vitem->box,
                                            6, vitem->rect.y + vitem->rect.height,
                                            vitem->rect.width - 12, -1,
                                            "anchor", GTK_ANCHOR_NW,
                                            NULL);
    vitem->rect.height += vitem->box->requisition.height;

    /* draw the separator line */
    vitem->rect.height += 12;
    line_path = g_strdup_printf ("M 6 %i L %i %i",
                                 vitem->rect.y + vitem->rect.height,
                                 vitem->rect.width - 6,
                                 vitem->rect.y + vitem->rect.height);
    vitem->separator = goo_canvas_path_new (vitem->group,
                                            line_path,
                                            "line-width", 1.0,
                                            "stroke-color", "grey",
                                            NULL);
    g_free (line_path);

    /* update items_height to reflect the addition of a new item */
    view->items_height += vitem->rect.height;

    /* reposition all items underneath */
    redraw_region.x = 0;
    redraw_region.y = vitem->rect.y;
    redraw_region.width = vitem->rect.width;
    redraw_region.height = vitem->rect.height;
    while (list_item != NULL) {
        vitem = (ViewItem *) list_item->data;
        vitem->rect.y = redraw_region.y + redraw_region.height;
        redraw_region.height += vitem->rect.height;
        list_item = g_list_next (list_item);
    }

    /* set the new canvas bounds */
    goo_canvas_set_bounds (GOO_CANVAS (view),
                           0.0, 0.0,
                           (gdouble) view->canvas_width,
                           (gdouble) view->items_height);

    /* redraw invalidated region */
    redraw_canvas_region (GOO_CANVAS(view), &redraw_region);
 }

#if 0
static void
remove_view_item (UndertowChannelView *view, ViewItem *vitem)
{
}
#endif

static void
resize_view_item (UndertowChannelView *view, GList *list_item)
{
    GdkRectangle redraw_region;
    ViewItem *vitem;

    vitem = (ViewItem *) list_item->data;

    redraw_region.x = 0;
    redraw_region.y = vitem->rect.y;
    redraw_region.width = view->canvas_width;
    redraw_region.height = 0;

    /* reposition all items underneath list_item */
    while (list_item != NULL) {
        GooCanvasBounds bounds;
        gchar *line_path;

        vitem = (ViewItem *) list_item->data;
        vitem->rect.y = redraw_region.y + redraw_region.height;
        vitem->rect.width = redraw_region.width;
        vitem->rect.height = 12;
        g_object_set (vitem->control,
                      "y", (gdouble) (vitem->rect.y + vitem->rect.height),
                      "width", (gdouble) (vitem->rect.width - 12),
                      NULL);
        vitem->rect.height += vitem->box->requisition.height;
        vitem->rect.height += 12;
        line_path = g_strdup_printf ("M 6 %i L %i %i",
                                     vitem->rect.y + vitem->rect.height,
                                     view->canvas_width - 6,
                                     vitem->rect.y + vitem->rect.height);
        g_object_set (vitem->separator, "data", line_path, NULL);
        g_free (line_path);
        redraw_region.height += vitem->rect.height;
        list_item = g_list_next (list_item);
    }

    /* redraw invalidated region */
    view->items_height = redraw_region.y + redraw_region.height;
    goo_canvas_set_bounds (GOO_CANVAS (view),
                           0.0, 0.0,
                           (gdouble) view->canvas_width,
                           (gdouble) view->items_height);
    redraw_canvas_region (GOO_CANVAS(view), &redraw_region);
    g_debug ("resize_view_item");
}

/*
 * draw the initial state of the widget
 */
static void
view_realize (GtkWidget *widget)
{
    UndertowChannelView *view = UNDERTOW_CHANNEL_VIEW (widget);
    UndertowEpisodeIter *iter;

    /* initialize the widget */
    GTK_WIDGET_CLASS (parent_class)->realize (widget);
    view->root_item = goo_canvas_get_root_item (GOO_CANVAS (view));
    view->items = NULL;
    view->canvas_width = widget->allocation.width;
    view->items_height = 0;
    g_signal_connect (view->channel, "episode-added", G_CALLBACK (on_episode_added), view);
    g_signal_connect (view->channel, "episode-expired", G_CALLBACK (on_episode_expired), view);
    
    /* add each episode to the view */
    iter = undertow_channel_get_episode_iter (view->channel);
    while (iter != NULL) {
        insert_view_item (view, undertow_episode_iter_get_episode (iter));
        iter = undertow_episode_iter_next (iter);
    }
}

static void
view_unrealize (GtkWidget *widget)
{
    UndertowChannelView *view = UNDERTOW_CHANNEL_VIEW (widget);
    GList *list_item;

    /* deallocate each list item */
    list_item = view->items;
    while (list_item != NULL) {
        ViewItem *item = (ViewItem *) list_item->data;
        g_object_unref (item->episode);
        g_free (item);
        list_item = g_list_next (list_item);
    }
    g_list_free (view->items);

    view->items = NULL;
    view->root_item = NULL;
    view->items_height = 0;
    view->canvas_width = 0;
}

static void
view_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
    GTK_WIDGET_CLASS (parent_class)->size_request (widget, requisition);
    requisition->width = 200;
    requisition->height = 200;
}

static void
view_size_allocate (GtkWidget *widget, GtkAllocation *alloc)
{
    UndertowChannelView *view = UNDERTOW_CHANNEL_VIEW (widget);

    /* run the parent method */
    GTK_WIDGET_CLASS (parent_class)->size_allocate (widget, alloc);
    if (!GTK_WIDGET_REALIZED (widget))
        return;
    if (view->canvas_width == alloc->width)
        return;

    /* set the new canvas width */
    view->canvas_width = alloc->width;

    /* resize each view item */
    if (view->items)
        resize_view_item (view, view->items);
}

static void
view_get_property (UndertowChannelView *        view,
                   guint                        property_id,
                   GValue *                     value,
                   GParamSpec *                 spec)
{
    switch (property_id) {
        case CHANNEL_VIEW_MODEL:
            g_value_set_object (value, view->channel);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (G_OBJECT (view), property_id, spec);
            break;
    }
}

static void 
view_set_property (UndertowChannelView *        view,
                   guint                        property_id,
                   const GValue *               value,
                   GParamSpec *                 spec)
{
    switch (property_id) {
        case CHANNEL_VIEW_MODEL:
            view->channel = g_value_dup_object (value);
            g_debug ("view_set_property");
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (G_OBJECT (view), property_id, spec);
            break;
    }
}

static void
view_dispose (UndertowChannelView *view)
{
    if (view->disposed)
        return;
    g_debug ("channel_view_dispose");
    G_OBJECT_CLASS (parent_class)->dispose (G_OBJECT (view));
    g_object_unref (view->channel);
    view->disposed = TRUE;
}

static void
view_class_init (UndertowChannelViewClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
    GParamSpec *pspec;

    parent_class = g_type_class_peek_parent (klass);

    object_class->get_property =
      (GObjectGetPropertyFunc) view_get_property;
    object_class->set_property =
      (GObjectSetPropertyFunc) view_set_property;
    object_class->dispose =
        (GObjectFinalizeFunc) view_dispose;
    widget_class->size_request = view_size_request;
    widget_class->size_allocate = view_size_allocate;
    widget_class->realize = view_realize;
    widget_class->unrealize = view_unrealize;

    pspec = g_param_spec_object ("channel", "channel model",
                                 "Get/set the channel model",
                                 UNDERTOW_TYPE_CHANNEL,
                                 G_PARAM_READWRITE|G_PARAM_CONSTRUCT_ONLY);
    g_object_class_install_property (object_class, CHANNEL_VIEW_MODEL, pspec);
}

GType
undertow_channel_view_get_type (void)
{
    static GType type = 0;

    if (!type) {
        static const GTypeInfo view_info = {
            sizeof (UndertowChannelViewClass),
            NULL,
            NULL,
            (GClassInitFunc) view_class_init,
            NULL,
            NULL,
            sizeof (UndertowChannelView),
            0,
            (GInstanceInitFunc) NULL,
            NULL
        };
        type = g_type_register_static (GOO_TYPE_CANVAS, 
                                       "UndertowChannelView",
                                       &view_info, 0);
    }
    return type;
}

UndertowChannelView *
undertow_channel_view_new_with_model (UndertowChannel *channel)
{
    GObject *object;

    object = g_object_new (UNDERTOW_TYPE_CHANNEL_VIEW, "channel", channel, NULL);
    return UNDERTOW_CHANNEL_VIEW (object);
}
