#include <glib.h>
#include <glib/gstdio.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <gconf/gconf-client.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libundertow.h>

#include "undertow-channel-manager.h"
#include "undertow-channel.h"
#include "undertow-stringops.h"
#include "undertow-g-error.h"

enum {
    MANAGER_CHANNEL_ADDED_SIGNAL,
    MANAGER_CHANNEL_REMOVED_SIGNAL,
    MANAGER_CHANNEL_ERROR_SIGNAL,
    N_MANAGER_SIGNALS
};
static guint manager_signals[N_MANAGER_SIGNALS];

static GObjectClass *parent_class = NULL;

static void
load_channel (lut_channel *c, UndertowChannelManager *manager)
{
    UndertowChannel *channel;

    channel = undertow_channel_new (c->url);
    g_hash_table_insert (manager->channels, g_strdup (c->url), channel);
    lut_channel_free (c);
}

static GObject *
manager_ctor (GType                     type,
              guint                     n_ctor_props,
              GObjectConstructParam *   ctor_props)
{
    UndertowChannelManagerClass *manager_class;
    GObjectClass *parent_class;

    manager_class = UNDERTOW_CHANNEL_MANAGER_CLASS (g_type_class_peek (UNDERTOW_TYPE_CHANNEL_MANAGER));
    if (manager_class->singleton) {
        g_object_ref (manager_class->singleton);
        return (GObject *) manager_class->singleton;
    }
    parent_class = G_OBJECT_CLASS (g_type_class_peek_parent (manager_class));
    manager_class->singleton = (UndertowChannelManager *)
        parent_class->constructor (type, n_ctor_props, ctor_props);
    return (GObject *) manager_class->singleton;
}

static void
manager_dispose (UndertowChannelManager *manager)
{
    if (manager->disposed)
        return;
    g_debug ("channel_manager_dispose");
    G_OBJECT_CLASS (parent_class)->dispose (G_OBJECT (manager));
    g_hash_table_destroy (manager->channels);
    manager->disposed = TRUE;
}

static void
manager_init (UndertowChannelManager *              manager,
              UndertowChannelManagerClass *         klass)
{
    manager->channels = g_hash_table_new_full (g_str_hash,
                                               g_str_equal,
                                               g_free,
                                               g_object_unref);
    lut_foreach_channel ((lut_foreach_channel_funct) load_channel, manager);
}

static void
manager_class_init (UndertowChannelManagerClass *klass)
{
    GObjectClass *gobject_class;

    parent_class = g_type_class_peek_parent (klass);

    gobject_class = G_OBJECT_CLASS (klass);
    gobject_class->constructor = manager_ctor;
    gobject_class->dispose = (GObjectFinalizeFunc) manager_dispose;

    manager_signals[MANAGER_CHANNEL_ADDED_SIGNAL] =
      g_signal_new ("channel-added",
                    G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST|G_SIGNAL_NO_RECURSE|G_SIGNAL_NO_HOOKS,
                    G_STRUCT_OFFSET (UndertowChannelManagerClass, channel_added),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__OBJECT,
                    G_TYPE_NONE, 1, UNDERTOW_TYPE_CHANNEL);
    manager_signals[MANAGER_CHANNEL_REMOVED_SIGNAL] =
      g_signal_new ("channel-removed",
                    G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST|G_SIGNAL_NO_RECURSE|G_SIGNAL_NO_HOOKS,
                    G_STRUCT_OFFSET (UndertowChannelManagerClass, channel_removed),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__OBJECT,
                    G_TYPE_NONE, 1, UNDERTOW_TYPE_CHANNEL);
    manager_signals[MANAGER_CHANNEL_ERROR_SIGNAL] =
      g_signal_new ("channel-error",
                    G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST|G_SIGNAL_NO_RECURSE|G_SIGNAL_NO_HOOKS,
                    G_STRUCT_OFFSET (UndertowChannelManagerClass, channel_error),
                    NULL, NULL,
                    undertow_cclosure_VOID__STRING_POINTER,
                    G_TYPE_NONE, 2, G_TYPE_STRING, G_TYPE_POINTER);
}

GType
undertow_channel_manager_get_type (void)
{
    static GType type = 0;

    if (!type) {
        static const GTypeInfo manager_info = {
            sizeof (UndertowChannelManagerClass),
            NULL,
            NULL,
            (GClassInitFunc) manager_class_init,
            NULL,
            NULL,
            sizeof (UndertowChannelManager),
            0,
            (GInstanceInitFunc) manager_init,
            NULL
        };
        type = g_type_register_static (G_TYPE_OBJECT, 
                                       "UndertowChannelManager",
                                       &manager_info, 0);
    }
    return type;
}

UndertowChannelManager *
undertow_channel_manager_get (void)
{
    return UNDERTOW_CHANNEL_MANAGER (g_object_new (UNDERTOW_TYPE_CHANNEL_MANAGER, NULL));
}

UndertowChannel *
undertow_channel_manager_get_channel (UndertowChannelManager *      manager,
                                      const gchar *                 url)
{
    UndertowChannel *channel;
   
    channel = UNDERTOW_CHANNEL (g_hash_table_lookup (manager->channels, url));
    if (channel == NULL)
        return NULL;
    g_object_ref (channel);
    return channel;
}

static void
on_channel_added (lut_channel *c, void *user_data)
{
    UndertowChannelManager *manager;
    UndertowChannel *channel;

    gdk_threads_enter ();

    manager = UNDERTOW_CHANNEL_MANAGER (user_data);
    channel = undertow_channel_new (c->url);
    g_hash_table_insert (manager->channels, g_strdup (c->url), channel);
    g_signal_emit (manager,
                   manager_signals[MANAGER_CHANNEL_ADDED_SIGNAL], 
                   0,
                   channel);
    lut_channel_free (c);
    g_object_unref (manager);

    gdk_flush ();
    gdk_threads_leave ();
}

static void
on_add_completed (lut_errno retval, void *user_data)
{
    UndertowChannelManager *manager;

    gdk_threads_enter ();

    manager = UNDERTOW_CHANNEL_MANAGER (user_data);
    if (retval != LUT_OK) {
        GError *error = g_error_new (UNDERTOW_G_ERROR,
                                     retval,
                                     lut_strerror (retval));
        g_signal_emit (manager,
                       manager_signals[MANAGER_CHANNEL_ERROR_SIGNAL], 
                       0,
                       NULL, error);
        g_error_free (error);
    }
    g_object_unref (manager);

    gdk_flush ();
    gdk_threads_leave ();
}

gboolean
undertow_channel_manager_add_channel (UndertowChannelManager *      manager,
                                      const gchar *                 url,
                                      GError **                     error)
{
    int retval;
   
    g_object_ref (manager);
    retval = lut_add_channel (url,
                              on_channel_added,
                              NULL,
                              NULL, 
                              on_add_completed,
                              manager);
    if (retval != LUT_OK) {
        g_object_unref (manager);
        g_critical ("failed to add channel: %s", lut_strerror (retval));
        return FALSE;
    }
    return TRUE;
}

void
undertow_channel_manager_refresh_channel (UndertowChannelManager *  manager,
                                          const gchar *             url)
{
    
    UndertowChannel *channel;
   
    channel = UNDERTOW_CHANNEL (g_hash_table_lookup (manager->channels, url));
    if (channel == NULL)
        g_critical ("undertow_channel_manager_refresh_channel: no such channel '%s'", url);
    else
        undertow_channel_refresh (channel);
}

typedef struct {
    UndertowChannelManager *manager;
    UndertowChannelManagerForeachFunc func;
    gpointer userdata;
} ForeachCtxt;

static void
foreach_channel (gchar *                key,
                 UndertowChannel *      channel,
                 ForeachCtxt *          ctxt)
{
    ctxt->func (ctxt->manager, channel, ctxt->userdata);
}

void
undertow_channel_manager_foreach_channel (UndertowChannelManager *              manager,
                                          UndertowChannelManagerForeachFunc     func,
                                          gpointer                              userdata)
{
    ForeachCtxt ctxt = { manager, func, userdata };

    g_hash_table_foreach (manager->channels, (GHFunc) foreach_channel, &ctxt);
}
