#ifndef UNDERTOW_CHANNEL_H
#define UNDERTOW_CHANNEL_H

#include <glib.h>
#include <glib-object.h>
#include <time.h>
#include <unistd.h>

#include "undertow-episode.h"

#define UNDERTOW_TYPE_CHANNEL             (undertow_channel_get_type())
#define UNDERTOW_CHANNEL(obj)             \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), UNDERTOW_TYPE_CHANNEL, UndertowChannel))
#define UNDERTOW_CHANNEL_CLASS(klass)     \
    (G_TYPE_CHECK_CLASS_CAST((klass), UNDERTOW_TYPE_CHANNEL, UndertowChannelClass))
#define UNDERTOW_IS_CHANNEL(obj)          \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), UNDERTOW_TYPE_CHANNEL))
#define UNDERTOW_IS_CHANNEL_CLASS(klass)  \
    (G_TYPE_CHECK_CLASS_TYPE((klass), UNDERTOW_TYPE_CHANNEL))
#define UNDERTOW_CHANNEL_GET_CLASS(obj)   \
    (G_TYPE_INSTANCE_GET_CLASS((obj), UNDERTOW_TYPE_CHANNEL, UndertowChannelClass))

typedef struct {
    GObject parent;
    gboolean disposed;
    gchar *title;
    gchar *url;
    gchar *description;
    time_t last_updated;
    time_t last_parsed;
    GSList *episodes;
    gboolean is_refreshing;
} UndertowChannel;

typedef struct {
    GObjectClass parent_class;
    void (*channel_updated)(UndertowChannel *);
    void (*episode_added)(UndertowChannel *, UndertowEpisode *);
    void (*episode_expired)(UndertowChannel *, UndertowEpisode *);
} UndertowChannelClass;

GType undertow_channel_get_type (void);

UndertowChannel *
undertow_channel_new (const gchar *url);

void
undertow_channel_refresh (UndertowChannel *channel);

void
undertow_channel_remove (UndertowChannel *channel);

typedef struct {
    GSList *ptr;
} UndertowEpisodeIter;

UndertowEpisodeIter *
undertow_channel_get_episode_iter (UndertowChannel *channel);

UndertowEpisodeIter *
undertow_episode_iter_next (UndertowEpisodeIter *iter);

UndertowEpisode *
undertow_episode_iter_get_episode (UndertowEpisodeIter *iter);

#endif
