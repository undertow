#ifndef UNDERTOW_CHANNEL_PAGE_H
#define UNDERTOW_CHANNEL_PAGE_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

#include "undertow-channel.h"
#include "undertow-channel-view.h"

#define UNDERTOW_TYPE_CHANNEL_PAGE             (undertow_channel_page_get_type())
#define UNDERTOW_CHANNEL_PAGE(obj)             \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), UNDERTOW_TYPE_CHANNEL_PAGE, UndertowChannelPage))
#define UNDERTOW_CHANNEL_PAGE_CLASS(klass)     \
    (G_TYPE_CHECK_CLASS_CAST((klass), UNDERTOW_TYPE_CHANNEL_PAGE, UndertowChannelPageClass))
#define UNDERTOW_IS_CHANNEL_PAGE(obj)          \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), UNDERTOW_TYPE_CHANNEL_PAGE))
#define UNDERTOW_IS_CHANNEL_PAGE_CLASS(klass)  \
    (G_TYPE_CHECK_CLASS_TYPE((klass), UNDERTOW_TYPE_CHANNEL_PAGE))
#define UNDERTOW_CHANNEL_PAGE_GET_CLASS(obj)   \
    (G_TYPE_INSTANCE_GET_CLASS((obj), UNDERTOW_TYPE_CHANNEL_PAGE, UndertowChannelPageClass))

typedef struct {
    GtkVBox parent;
    gchar *channel_url;
    GtkWidget *title;
    GtkWidget *image;
    UndertowChannelView *view;
    UndertowChannel *channel;
    gboolean disposed;
} UndertowChannelPage;

typedef struct {
    GtkVBoxClass parent_class;
} UndertowChannelPageClass;

GType undertow_channel_page_get_type (void);

GtkWidget *undertow_channel_page_new (const gchar *url);

#endif
