#ifndef UNDERTOW_PLAYER_H
#define UNDERTOW_PLAYER_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <gst/gst.h>
#include <gdk-pixbuf/gdk-pixbuf.h>

#define UNDERTOW_TYPE_PLAYER             (undertow_player_get_type())
#define UNDERTOW_PLAYER(obj)             \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), UNDERTOW_TYPE_PLAYER, UndertowPlayer))
#define UNDERTOW_PLAYER_CLASS(klass)     \
    (G_TYPE_CHECK_CLASS_CAST((klass), UNDERTOW_TYPE_PLAYER, UndertowPlayerClass))
#define UNDERTOW_IS_PLAYER(obj)          \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), UNDERTOW_TYPE_PLAYER))
#define UNDERTOW_IS_PLAYER_CLASS(klass)  \
    (G_TYPE_CHECK_CLASS_TYPE((klass), UNDERTOW_TYPE_PLAYER))
#define UNDERTOW_PLAYER_GET_CLASS(obj)   \
    (G_TYPE_INSTANCE_GET_CLASS((obj), UNDERTOW_TYPE_PLAYER, UndertowPlayerClass))

typedef enum {
    UNDERTOW_PLAYER_STATE_IS_PLAYING,
    UNDERTOW_PLAYER_STATE_IS_PAUSED,
    UNDERTOW_PLAYER_STATE_IS_STOPPED
} UndertowPlayerState;

typedef struct {
    GtkVBox parent;
    GdkPixbuf *default_image;
    GtkWidget *details;
    GtkWidget *video;
    GstElement *controller;
    GstElement *videosink;
    GstElement *audiosink;
    GtkWidget *play_image;
    GtkWidget *pause_image;
    GtkWidget *playpause_button;
    GtkWidget *stop_button;
    GtkWidget *prev_button;
    GtkWidget *next_button;
    GtkWidget *volume_button;
    GtkWidget *status;
    GtkWidget *seekbar;
    gint update_id;
    gboolean is_seeking;
} UndertowPlayer;

struct _UndertowPlayerClass {
    GtkVBoxClass parent_class;
    void (*player_state_changed)(UndertowPlayer *);
};
typedef struct _UndertowPlayerClass UndertowPlayerClass;

GType undertow_player_get_type (void);

GtkWidget *undertow_player_new (void);
gboolean undertow_player_play (UndertowPlayer *player, gchar *uri, gchar *title);
gboolean undertow_player_pause (UndertowPlayer *player);
gboolean undertow_player_stop (UndertowPlayer *player);

#endif
