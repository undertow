#include <string.h>
#include <ctype.h>
#include <glib.h>

static void
parse_start_element (GMarkupParseContext *      ctxt,
                     const gchar *              element_name,
                     const gchar **             attribute_name,
                     const gchar **             attribute_vals,
                     gpointer                   user_data,
                     GError **                  error)
{
    if (!strcasecmp (element_name, "br"))
        g_string_append ((GString *) user_data, "\n");
    else if (!strcasecmp (element_name, "p"))
        g_string_append ((GString *) user_data, "\n\n");
}

static void
parse_end_element (GMarkupParseContext *        ctxt,
                   const gchar *                element_name,
                   gpointer                     user_data,
                   GError **                    error)
{
    if (!strcasecmp (element_name, "p"))
        g_string_append ((GString *) user_data, "\n\n");
}

static void
parse_text (GMarkupParseContext *       ctxt,
            const gchar *               text,
            gsize                       text_len,
            gpointer                    user_data,
            GError **                   error)
{
    g_string_append_len ((GString *) user_data, text, text_len);
}

static gchar *
strip_html (const gchar *dirty)
{
    GMarkupParser parser =
    {
        .start_element = parse_start_element,
        .end_element = parse_end_element,
        .text = parse_text
    };
    GMarkupParseContext *ctxt;
    GString *clean;
    GError *error = NULL;

    if (dirty == NULL)
        return NULL;

    clean = g_string_new (NULL);
    ctxt = g_markup_parse_context_new (&parser,
                                       G_MARKUP_TREAT_CDATA_AS_TEXT,
                                       clean,
                                       NULL);
    g_markup_parse_context_parse (ctxt, dirty, strlen (dirty), &error);
    g_markup_parse_context_free (ctxt);
    if (error) {
        g_critical ("failed to strip html: %s", error->message);
        g_error_free (error);
        g_string_free (clean, TRUE);
        return g_strdup (dirty);
    }
    return g_string_free (clean, FALSE);
}

static gchar *
strip_whitespace (const gchar *dirty)
{
    GString *clean;
    gint dirty_len, i;
    gchar prev = 0, prev2 = 0;

    if (dirty == NULL)
        return NULL;

    clean = g_string_new (NULL);
    dirty_len = strlen (dirty);
    for (i = 0; i < dirty_len; i++) {
        if (dirty[i] == '\f' || dirty[i] == '\r' || dirty[i] == '\t' || dirty[i] == '\v')
            continue;
        else if (dirty[i] == '\n') {
            if (prev == '\n' && prev2 != '\n')
                g_string_append (clean, "\n\n");
        }
        else if (dirty[i] == ' ') {
            if (prev != ' ' && prev != '\n')
                g_string_append_c (clean, dirty[i]);
        }
        else
            g_string_append_c (clean, dirty[i]);
        prev2 = prev;
        prev = dirty[i];
    }

    return g_string_free (clean, FALSE);
}

gchar *
undertow_string_clean (const gchar *dirty)
{
    if (dirty == NULL)
        return NULL;
    /*
    gchar *pass1, *pass2;
    pass1 = strip_html (dirty);
    if (pass1 == NULL);
        return NULL;
    pass2 = strip_whitespace (pass1);
    g_free (pass1);
    return pass2;
    */
    return strip_whitespace (dirty);
}
