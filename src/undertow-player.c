#include <string.h>
#include <glib.h>
#include <glib-object.h>
#include <gst/gst.h>
#include <gst/interfaces/xoverlay.h>
#include <gdk/gdkx.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gconf/gconf-client.h>

#include "undertow-player.h"

enum {
    PLAYER_PLAYLIST = 1,
    PLAYER_URI,
    PLAYER_STATE, 
    PLAYER_POSITION,
    PLAYER_DURATION,
    PLAYER_VOLUME
};

enum {
    SIGNAL_PLAYER_STATE_CHANGED,
    N_SIGNALS
};
static guint signals[N_SIGNALS];

static GObjectClass *parent_class = NULL;

static void
player_clicked_play (GtkToolButton *play, UndertowPlayer *player)
{
    if (GST_STATE (player->controller) == GST_STATE_PLAYING)
        undertow_player_pause (player);
    else
        undertow_player_play (player, NULL, NULL);
    g_signal_emit (player,
                   signals[SIGNAL_PLAYER_STATE_CHANGED],
                   0, NULL);
}

static void
player_clicked_stop (GtkToolButton *stop, UndertowPlayer *player)
{
    undertow_player_stop (player);
    g_signal_emit (player,
                   signals[SIGNAL_PLAYER_STATE_CHANGED],
                   0, NULL);
}

static void
player_volume_changed (GtkVolumeButton *                volume,
                       gdouble                          value,
                       UndertowPlayer *                 player)
{
    g_object_set (player, "volume", value, NULL);
}


static gboolean
player_status_update (UndertowPlayer *player)
{
    gint position, duration;
    gchar *status;
    
    if (player->is_seeking)
        return TRUE;
    g_object_get (player,
                  "position", &position,
                  "duration", &duration,
                  NULL);
    if (duration <= 0)
        return TRUE;
    
    gtk_range_set_range (GTK_RANGE (player->seekbar),
                         0.0, (gdouble) duration);
    gtk_range_set_value (GTK_RANGE (player->seekbar),
                         (gdouble) position);
    position = position / 1000;
    duration = duration / 1000;
    status = g_strdup_printf ("%i:%.2i of %i:%.2i",
                              position / 60,
                              position % 60,
                              duration / 60,
                              duration % 60);
    gtk_label_set_text(GTK_LABEL(player->status), status);
    g_free(status);
    return TRUE;
}


static void
player_seekbar_changed (GtkRange *range, UndertowPlayer *player)
{
    GtkAdjustment *adj;
    gdouble value;
    gint position;
    gchar *status;
    
    adj = gtk_range_get_adjustment (range);
    g_object_get (adj, "value", &value, NULL);
    position = ((gint) value) / 1000;
    status = g_strdup_printf ("Seeking to %i:%.2i",
                              position / 60,
                              position % 60);
    gtk_label_set_text(GTK_LABEL(player->status), status);
    g_free(status);
}

static gboolean
player_seekbar_button_press_event (GtkWidget *widget,
                                   GdkEventButton *event,
                                   UndertowPlayer *player)
{
    player->is_seeking = TRUE;
    gtk_range_set_update_policy (GTK_RANGE (player->seekbar),
                                 GTK_UPDATE_CONTINUOUS);
    return FALSE;
}

static gboolean
player_seekbar_button_release_event (GtkWidget *widget,
                                     GdkEventButton *event,
                                     UndertowPlayer *player)
{
    gint64 position;
    
    gtk_range_set_update_policy (GTK_RANGE (player->seekbar),
                                 GTK_UPDATE_DISCONTINUOUS);
    player->is_seeking = FALSE;
    position = (gint64) gtk_range_get_value (GTK_RANGE (player->seekbar));
    if (!gst_element_seek (player->controller, 1.0,
                           GST_FORMAT_TIME, GST_SEEK_FLAG_FLUSH,
                           GST_SEEK_TYPE_SET, position * 1000000,
                           GST_SEEK_TYPE_NONE,
                           GST_CLOCK_TIME_NONE))
        g_warning ("gst_element_seek() failed");
    return FALSE;
}

static gboolean
player_bus_message(GstBus *bus,
                   GstMessage *message,
                   UndertowPlayer *player)
{
    GtkWidget *dialog;
    GError *error;
    gchar *string;
    const GstStructure *s;
    gint percent;
    
    switch (GST_MESSAGE_TYPE(message)) {
        case GST_MESSAGE_ERROR:
            gst_message_parse_error (message, &error, &string);
            g_debug ("error playing stream: %s", string);
            g_free (string);
            dialog = gtk_message_dialog_new_with_markup (NULL,
                                                         GTK_DIALOG_DESTROY_WITH_PARENT,
                                                         GTK_MESSAGE_ERROR,
                                                         GTK_BUTTONS_CLOSE,
                                                         "<b>Playback failed</b>\n%s",
                                                         error->message);
            g_error_free (error);
            gtk_dialog_run (GTK_DIALOG (dialog));
            gtk_widget_destroy (dialog);
            undertow_player_stop (player);
            /*
            gtk_button_set_image (GTK_BUTTON (player->playpause_button), player->play_image);
            */
            break;
        case GST_MESSAGE_WARNING:
            gst_message_parse_warning(message, &error, &string);
            g_debug("gstreamer warning: %s (debug: %s)", error->message, string);
            g_error_free(error);
            g_free(string);
            break;
        case GST_MESSAGE_BUFFERING:
            s = gst_message_get_structure (message);
            gst_structure_get_int (s, "buffer-percent", &percent);
            string = g_strdup_printf ("Buffering: %i%%", percent);
            gtk_label_set_text (GTK_LABEL (player->status), string);
            g_free (string);
            break;
        case GST_MESSAGE_EOS:
            g_debug("-- end of stream --");
            undertow_player_stop (player);
            g_signal_emit (player,
                           signals[SIGNAL_PLAYER_STATE_CHANGED],
                           0, NULL);
            break;
        default:
            break;
    }
    return TRUE;    /* remove message from the queue */
}

static GstBusSyncReply
attach_gst_to_window (GstBus *bus, GstMessage *message, UndertowPlayer *player)
{
    if (GST_MESSAGE_TYPE (message) != GST_MESSAGE_ELEMENT)
        return GST_BUS_PASS;
    if (!gst_structure_has_name (message->structure, "prepare-xwindow-id"))
        return GST_BUS_PASS;
    gst_x_overlay_set_xwindow_id (GST_X_OVERLAY (GST_MESSAGE_SRC (message)),
                                  GDK_WINDOW_XID (player->video->window));
    g_debug ("attach_gst_to_window");
    gst_message_unref (message);
    return GST_BUS_DROP;
}

static void
player_get_property (UndertowPlayer *player,
                     guint property_id,
                     GValue *value,
                     GParamSpec *spec)
{
    gchar *uri;
    gint64 i64;
    GstFormat fmt;
    gdouble volume;
    
    switch (property_id) {
        case PLAYER_URI:
            g_object_get (player->controller, "location", &uri, NULL);
            g_value_take_string (value, uri);
            break;
        case PLAYER_STATE:
            switch (GST_STATE (player->controller)) {
                case GST_STATE_PLAYING:
                    g_value_set_int (value, UNDERTOW_PLAYER_STATE_IS_PLAYING);
                    break;
                case GST_STATE_NULL:
                    g_value_set_int (value, UNDERTOW_PLAYER_STATE_IS_STOPPED);
                    break;
                case GST_STATE_PAUSED:
                case GST_STATE_READY:
                    g_value_set_int (value, UNDERTOW_PLAYER_STATE_IS_PAUSED);
                    break;
                case GST_STATE_VOID_PENDING:
                    g_value_set_int (value, UNDERTOW_PLAYER_STATE_IS_STOPPED);
                    break;
            }
            break;
        case PLAYER_POSITION:
            fmt = GST_FORMAT_TIME;
            if (!gst_element_query_position (player->controller, &fmt, &i64)) {
                g_value_set_int (value, -1);
                break;
            }
            g_value_set_int (value, (i64 / 1000000));
            break;
        case PLAYER_DURATION:
            fmt = GST_FORMAT_TIME;
            if (!gst_element_query_duration (player->controller, &fmt, &i64)) {
                g_value_set_int (value, -1);
                break;
            }
            g_value_set_int (value, (i64 / 1000000));
            break;
        case PLAYER_VOLUME:
            g_object_get (player->controller, "volume", &volume, NULL);
            g_value_set_double (value, volume);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (G_OBJECT (player),
                                               property_id,
                                               spec);
            break;
    }
}

static void 
player_set_property (UndertowPlayer *player,
                     guint property_id,
                     const GValue *value,
                     GParamSpec *spec)
{
    const gchar *uri;
    gint64 position;
    
    switch (property_id) {
        case PLAYER_URI:
            uri = g_value_get_string (value);
            g_object_set (G_OBJECT (player->controller), "uri", uri, NULL);
            break;
        case PLAYER_POSITION:
            if (GST_STATE (player->controller) != GST_STATE_PLAYING &&
                GST_STATE (player->controller) != GST_STATE_PAUSED) {
                g_critical ("failed to set property 'position': no track loaded");
                break;
            }
            position = g_value_get_int (value) * 1000000;
            if (!gst_element_seek (player->controller,
                                   1.0,
                                   GST_FORMAT_TIME,
                                   GST_SEEK_FLAG_FLUSH,
                                   GST_SEEK_TYPE_SET, position,
                                   GST_SEEK_TYPE_NONE,
                                   GST_CLOCK_TIME_NONE))
                g_critical ("failed to set property 'position': gst_element_seek() failed");
            break;
        case PLAYER_VOLUME:
            g_object_set (player->controller,
                          "volume", g_value_get_double (value),
                          NULL);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (G_OBJECT (player),
                                               property_id,
                                               spec);
            break;
    }
}

static gboolean
needs_redraw (GtkWidget *w, GdkEvent *ev, UndertowPlayer *player)
{
    if (player->videosink && GST_IS_X_OVERLAY (player->videosink)) {
        gst_x_overlay_expose (GST_X_OVERLAY (player->videosink));
        return FALSE;
    }
    gdk_window_clear_area (w->window,
                           0, 0,
                           w->allocation.width,
                           w->allocation.height);
    return TRUE;
}

static void
video_window_realized (GtkWidget *w, UndertowPlayer *player)
{
    GdkColor color;

    gdk_color_parse ("black", &color);
    gdk_colormap_alloc_color (gtk_widget_get_colormap (w), &color, TRUE, TRUE);
    gdk_window_set_background (w->window, &color);
}

static void 
player_init (UndertowPlayer *player, UndertowPlayerClass *klass)
{
    GtkWidget *toolbar, *image;
    GstBus *bus;
    GConfClient *gconf;
    GtkAdjustment *adj;
    gchar *audiosink, *videosink;
    gdouble volume;
    GError *error = NULL;

    gconf = gconf_client_get_default ();
    audiosink = gconf_client_get_string (gconf,
                                         "/apps/undertow/audiosink",
                                         &error);
    if (error)
        g_critical ("failed to read audiosink setting: %s", error->message);
    videosink = gconf_client_get_string (gconf,
                                         "/apps/undertow/videosink",
                                         &error);
    if (error)
        g_critical ("failed to read videosink setting: %s", error->message);
    volume = (gdouble) gconf_client_get_float (gconf,
                                               "/apps/undertow/volume",
                                               &error);
    if (error)
        g_critical ("failed to read volume setting: %s", error->message);

    player->update_id = -1;
    player->video = gtk_drawing_area_new ();
    gtk_box_pack_start (GTK_BOX (player), player->video, TRUE, TRUE, 0);
    
    /* add the seek bar */
    player->seekbar = gtk_hscale_new_with_range (0, 1, 1);
    gtk_scale_set_draw_value (GTK_SCALE (player->seekbar), FALSE);
    gtk_widget_set_sensitive (player->seekbar, FALSE);
    gtk_range_set_update_policy (GTK_RANGE (player->seekbar),
                                 GTK_UPDATE_DISCONTINUOUS);
    g_signal_connect (player->seekbar, "value-changed",
                      G_CALLBACK (player_seekbar_changed), player);
    g_signal_connect (player->seekbar, "button-press-event",
                      G_CALLBACK (player_seekbar_button_press_event),
                      player);
    g_signal_connect (player->seekbar, "button-release-event",
                      G_CALLBACK (player_seekbar_button_release_event),
                      player);
    gtk_box_pack_start (GTK_BOX (player), player->seekbar, FALSE, FALSE, 3);
 
    /* add the player toolbar */
    toolbar = gtk_hbox_new (FALSE, 0);
    gtk_box_pack_start (GTK_BOX (player), toolbar, FALSE, FALSE, 0);

    /* hold references to pause and play stock images */
    player->play_image =
        gtk_image_new_from_stock (GTK_STOCK_MEDIA_PLAY, GTK_ICON_SIZE_SMALL_TOOLBAR);
    g_object_ref (player->play_image);
    player->pause_image =
        gtk_image_new_from_stock (GTK_STOCK_MEDIA_PAUSE, GTK_ICON_SIZE_SMALL_TOOLBAR);
    g_object_ref (player->pause_image);

    /* add the toolbar buttons */
    player->prev_button = gtk_button_new ();
    image = gtk_image_new_from_stock (GTK_STOCK_MEDIA_PREVIOUS, GTK_ICON_SIZE_SMALL_TOOLBAR);
    gtk_button_set_image (GTK_BUTTON (player->prev_button), image);
    /* g_object_unref (image); */
    gtk_button_set_relief (GTK_BUTTON (player->prev_button), GTK_RELIEF_NONE);
    gtk_box_pack_start (GTK_BOX (toolbar), player->prev_button, FALSE, FALSE, 2);
    
    player->playpause_button = gtk_button_new ();
    gtk_button_set_image (GTK_BUTTON (player->playpause_button), player->play_image);
    gtk_button_set_relief (GTK_BUTTON (player->playpause_button), GTK_RELIEF_NONE);
    g_signal_connect (player->playpause_button, "clicked", G_CALLBACK (player_clicked_play), player);
    gtk_box_pack_start (GTK_BOX (toolbar), player->playpause_button, FALSE, FALSE, 2);
    
    player->stop_button = gtk_button_new ();
    image = gtk_image_new_from_stock (GTK_STOCK_MEDIA_STOP, GTK_ICON_SIZE_SMALL_TOOLBAR);
    gtk_button_set_image (GTK_BUTTON (player->stop_button), image);
    gtk_button_set_relief (GTK_BUTTON (player->stop_button), GTK_RELIEF_NONE);
    gtk_widget_set_sensitive (player->stop_button, FALSE);
    g_signal_connect (player->stop_button, "clicked", G_CALLBACK (player_clicked_stop), player);
    gtk_box_pack_start (GTK_BOX (toolbar), player->stop_button, FALSE, FALSE, 2);
    
    player->next_button = gtk_button_new ();
    image = gtk_image_new_from_stock (GTK_STOCK_MEDIA_NEXT, GTK_ICON_SIZE_SMALL_TOOLBAR);
    gtk_button_set_image (GTK_BUTTON (player->next_button), image);
    gtk_button_set_relief (GTK_BUTTON (player->next_button), GTK_RELIEF_NONE);
    gtk_box_pack_start (GTK_BOX (toolbar), player->next_button, FALSE, FALSE, 2);
    
    gtk_box_pack_start (GTK_BOX (toolbar), gtk_vseparator_new (), FALSE, FALSE, 2);
    
    player->volume_button = gtk_volume_button_new ();
    adj = GTK_ADJUSTMENT (gtk_adjustment_new (volume, 0.0, 10.0, 0.2, 1.0, 0.0));
    gtk_scale_button_set_adjustment (GTK_SCALE_BUTTON (player->volume_button), adj);
    gtk_button_set_relief (GTK_BUTTON (player->volume_button), GTK_RELIEF_NONE);
    g_signal_connect (player->volume_button, "value-changed", G_CALLBACK (player_volume_changed), player);
    gtk_box_pack_start (GTK_BOX (toolbar), GTK_WIDGET (player->volume_button), FALSE, FALSE, 2);

    gtk_box_pack_start (GTK_BOX (toolbar), gtk_vseparator_new (), FALSE, FALSE, 2);
    
    /* add the status display */
    player->status = gtk_label_new ("Not playing");
    gtk_misc_set_padding (GTK_MISC (player->status), 12, 3);
    gtk_box_pack_start (GTK_BOX (toolbar), player->status, TRUE, TRUE, 2);
   
    /* create new gstreamer playbin element */
    player->controller = gst_element_factory_make ("playbin", "play");
    player->videosink = gst_element_factory_make (videosink, "videosink");
    player->audiosink = gst_element_factory_make (audiosink, "videosink");
    g_object_set (player->videosink, "force-aspect-ratio", TRUE, NULL);
    g_object_set (player->controller,
                  "video-sink", player->videosink,
                  "audio-sink", player->audiosink,
                  "volume", volume,
                  NULL);
    if (GST_IS_X_OVERLAY (player->videosink)) {
        g_signal_connect (player->video, "realize", G_CALLBACK (video_window_realized), player);
        g_signal_connect (player->video, "expose-event", G_CALLBACK (needs_redraw), player);
        g_signal_connect (player->video, "configure-event", G_CALLBACK (needs_redraw), player);
    }
    else
        g_debug ("videosink is not an X overlay!");
    bus = gst_pipeline_get_bus (GST_PIPELINE (player->controller));
    gst_bus_set_sync_handler (bus, (GstBusSyncHandler) attach_gst_to_window, player);
    gst_bus_add_watch (bus, (GstBusFunc) player_bus_message, player);

    g_object_unref (gconf);
    g_free (videosink);
    g_free (audiosink);
}

static void
player_dispose (UndertowPlayer *player)
{
    if (player->controller) {
        gst_element_set_state (player->controller, GST_STATE_NULL);
        gst_object_unref (GST_OBJECT (player->controller));
        player->controller = NULL;
    }
    parent_class->dispose (G_OBJECT (player));
}

static void 
player_class_init (UndertowPlayerClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS(klass);
    GParamSpec *pspec;
    
    parent_class = g_type_class_peek_parent (klass);
    
    /* override GObject methods */
    object_class->get_property =
      (GObjectGetPropertyFunc) player_get_property;
    object_class->set_property =
      (GObjectSetPropertyFunc) player_set_property;
    object_class->dispose =
      (GObjectFinalizeFunc) player_dispose;

    /* set player properties */
    pspec = g_param_spec_string ("uri",
                                 "stream URI",
                                 "Get/set the stream URI",
                                 NULL,
                                 G_PARAM_READABLE);
    g_object_class_install_property (object_class,
                                     PLAYER_URI,
                                     pspec);
    pspec = g_param_spec_int ("state",
                              "Player state",
                              "Get the current player state",
                              UNDERTOW_PLAYER_STATE_IS_PLAYING,
                              UNDERTOW_PLAYER_STATE_IS_STOPPED,
                              UNDERTOW_PLAYER_STATE_IS_STOPPED,
                              G_PARAM_READABLE);
    g_object_class_install_property (object_class,
                                     PLAYER_STATE,
                                     pspec);
    pspec = g_param_spec_int ("position",
                              "Position in stream (in ms)",
                              "Get/set the stream position",
                              0, G_MAXINT, 0,
                              G_PARAM_READWRITE);
    g_object_class_install_property (object_class,
                                     PLAYER_POSITION,
                                     pspec);
    pspec = g_param_spec_int ("duration",
                              "Length of the stream (in ms)",
                              "Get the stream length",
                              0, G_MAXINT, 0,
                              G_PARAM_READABLE);
    g_object_class_install_property (object_class,
                                     PLAYER_DURATION,
                                     pspec);
    pspec = g_param_spec_double ("volume",
                                 "Player volume",
                                 "Get/set the volume",
                                 0.0, 10.0, 4.0,
                                 G_PARAM_READWRITE);
    g_object_class_install_property (object_class,
                                     PLAYER_VOLUME,
                                     pspec);

    /* create signals */
    signals[SIGNAL_PLAYER_STATE_CHANGED] =
      g_signal_new ("player-state-changed",
                    G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_FIRST|G_SIGNAL_NO_RECURSE|G_SIGNAL_NO_HOOKS,
                    G_STRUCT_OFFSET (UndertowPlayerClass, player_state_changed),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__VOID,
                    G_TYPE_NONE, 0);
}

/********************************/
/* BELOW HERE IS THE PUBLIC API */
/********************************/

GType
undertow_player_get_type(void)
{
    static GType type = 0;
    if (type == 0) {
        static const GTypeInfo info = {
            sizeof (UndertowPlayerClass),
            NULL,
            NULL,
            (GClassInitFunc) player_class_init,
            NULL,
            NULL,
            sizeof (UndertowPlayer),
            0,
            (GInstanceInitFunc) player_init
        };
        type = g_type_register_static (GTK_TYPE_VBOX,
                                       "UndertowPlayerType",
                                       &info, 0);
    }
    return type;
}

GtkWidget *
undertow_player_new (void)
{
    return g_object_new (UNDERTOW_TYPE_PLAYER, NULL);
}

gboolean
undertow_player_play (UndertowPlayer *player, gchar *uri, gchar *title)
{
    GstState state;
    
    g_return_val_if_fail(UNDERTOW_IS_PLAYER (player), FALSE);
    
    state = GST_STATE (player->controller);
    if (uri != NULL) {
        gst_element_set_state (player->controller, GST_STATE_NULL);
        g_object_set (player->controller, "uri", uri, NULL);
    }
    gst_element_set_state (player->controller, GST_STATE_PLAYING);
    
    /* change the relevant interface elements */
    if (title)
        gtk_label_set_text (GTK_LABEL (player->details), title);
    gtk_widget_set_sensitive (player->stop_button, TRUE);
    gtk_button_set_image (GTK_BUTTON (player->playpause_button), player->pause_image);

    gtk_widget_set_sensitive (player->seekbar, TRUE);
    if (player->update_id < 0)
        player->update_id = g_timeout_add (100,
                                           (GSourceFunc) player_status_update,
                                           player);
    g_debug("-- play --");
    return TRUE;
}

gboolean
undertow_player_pause (UndertowPlayer *player)
{
    g_return_val_if_fail (UNDERTOW_IS_PLAYER (player), FALSE);
    
    gst_element_set_state (player->controller, GST_STATE_PAUSED);
    gtk_button_set_image (GTK_BUTTON (player->playpause_button), player->play_image);
    if (player->update_id >= 0) {
        g_source_remove (player->update_id);
        player->update_id = -1;
    }
    g_debug("-- pause --");
    return TRUE;
}

gboolean
undertow_player_stop (UndertowPlayer *player)
{
    g_return_val_if_fail (UNDERTOW_IS_PLAYER (player), FALSE);
    
    gst_element_set_state (player->controller, GST_STATE_NULL);
    gtk_widget_set_sensitive (GTK_WIDGET (player->stop_button), FALSE);
    gtk_range_set_value (GTK_RANGE (player->seekbar), 0);
    gtk_widget_set_sensitive (player->seekbar, FALSE);
    gtk_label_set_text (GTK_LABEL (player->status), "Not Playing");
    gtk_button_set_image (GTK_BUTTON (player->playpause_button), player->play_image);
    if (player->update_id >= 0) {
        g_source_remove (player->update_id);
        player->update_id = -1;
    }
    g_debug("-- stop --");
    return TRUE;
}
