#include <gtk/gtk.h>
#include <glade/glade-xml.h>

#include "undertow-main-window.h"
#include "undertow-workspace-switcher.h"
#include "undertow-location-chooser.h"
#include "undertow-location-model.h"
#include "undertow-channel-page.h"
#include "undertow-player.h"

static GladeXML *ui = NULL;
static UndertowLocationModel *model = NULL;

static void
on_add_channel (GtkMenuItem *               item,
                UndertowLocationModel *     model)
{
    undertow_location_model_add_channel (model);
}

static void
on_add_folder (GtkMenuItem *                item,
               UndertowLocationModel *      model)
{
    undertow_location_model_add_folder (model);
}

static void
on_add_playlist (GtkMenuItem *              item,
                 UndertowLocationModel *    model)
{
    undertow_location_model_add_playlist (model);
}

static void
on_edit_location (GtkMenuItem *                 item,
                  UndertowLocationChooser *     chooser)
{
    undertow_location_chooser_edit_current (chooser);
}

static void
on_refresh_location (GtkMenuItem *              item,
                     UndertowLocationChooser *  chooser)
{
    undertow_location_chooser_refresh_current (chooser);
}

static void
on_remove_location (GtkMenuItem *               item,
                    UndertowLocationChooser *   chooser)
{
    undertow_location_chooser_remove_current (chooser);
}

static void
on_location_changed (UndertowLocationChooser *      chooser,
                     gchar *                        url,
                     UndertowWorkspaceSwitcher *    switcher)
{
    g_debug ("switching to %s", url);
    undertow_workspace_switcher_set_page (switcher, url);
}

static gboolean
add_location_page (UndertowLocationModel *      model,
                   GtkTreePath *                tree_path,
                   GtkTreeIter *                iter,
                   UndertowWorkspaceSwitcher *  switcher)
{
    UndertowLocationType type;
    gchar *url;
    GtkWidget *page;

    gtk_tree_model_get (GTK_TREE_MODEL (model), iter,
                        UNDERTOW_LOCATION_MODEL_TYPE_COLUMN, &type,
                        UNDERTOW_LOCATION_MODEL_URL_COLUMN, &url,
                        -1);
    if (type == UNDERTOW_LOCATION_TYPE_CHANNEL) {
        page = undertow_channel_page_new (url);
        undertow_workspace_switcher_add_page (switcher, url, page);
        g_debug ("added location %s to switcher", url);
    }
    g_free (url);
    return FALSE;
}

GtkWidget *
undertow_main_window_create (void)
{
    GtkWidget *window;
    GtkWidget *workspace_container;
    GtkWidget *hpaned;
    GtkWidget *location_chooser;
    GtkWidget *switcher;
    GtkWidget *menuitem;
    GtkWidget *align;
    GtkWidget *frame;
    GtkWidget *player;

    model = undertow_location_model_get ();
    /*
    g_signal_connect (model, "location-added", G_CALLBACK (on_location_added), switcher);
    g_signal_connect (model, "location-removed", G_CALLBACK (on_location_removed), switcher);
    */

    /* this function should only be called once */
    g_assert (ui == NULL);

    ui = glade_xml_new (GLADEDIR "/main-window.glade", NULL, NULL);

    /* get the toplevel window */
    window = glade_xml_get_widget (ui, "main-window");
    g_signal_connect (window, "delete-event", G_CALLBACK (gtk_main_quit), NULL);

    /* create child widgets */
    workspace_container = glade_xml_get_widget (ui, "workspace-container");
    hpaned = gtk_hpaned_new ();
    gtk_container_add (GTK_CONTAINER (workspace_container), hpaned);

    /* chooser pane */
    align = gtk_alignment_new (0.5, 0.5, 1.0, 1.0);
    gtk_alignment_set_padding (GTK_ALIGNMENT (align), 6, 6, 3, 0);
    gtk_paned_add1 (GTK_PANED (hpaned), align);
    frame = gtk_frame_new (NULL);
    gtk_container_add (GTK_CONTAINER (align), frame);
    location_chooser = undertow_location_chooser_new ();
    gtk_widget_set_size_request (GTK_WIDGET (location_chooser), 200, -1);
    gtk_container_add (GTK_CONTAINER (frame), location_chooser);

    /* workspace pane */
    align = gtk_alignment_new (0.5, 0.5, 1.0, 1.0);
    gtk_alignment_set_padding (GTK_ALIGNMENT (align), 6, 6, 0, 3);
    gtk_paned_add2 (GTK_PANED (hpaned), align);
    frame = gtk_frame_new (NULL);
    gtk_container_add (GTK_CONTAINER (align), frame);
    switcher = undertow_workspace_switcher_new ();
    gtk_container_add (GTK_CONTAINER (frame), switcher);

    /* switch to the appropriate workspace when the location changes */
    g_signal_connect (location_chooser, "location-changed",
                      G_CALLBACK (on_location_changed), switcher);

    /* menu items */
    menuitem = glade_xml_get_widget (ui, "file-quit-menuitem");
    g_signal_connect (menuitem, "activate", G_CALLBACK (gtk_main_quit), NULL);
    menuitem = glade_xml_get_widget (ui, "channel-add-menuitem");
    g_signal_connect (menuitem, "activate", G_CALLBACK (on_add_channel), model);
    menuitem = glade_xml_get_widget (ui, "folder-add-menuitem");
    g_signal_connect (menuitem, "activate", G_CALLBACK (on_add_folder), model);
    menuitem = glade_xml_get_widget (ui, "playlist-add-menuitem");
    g_signal_connect (menuitem, "activate", G_CALLBACK (on_add_playlist), model);
    menuitem = glade_xml_get_widget (ui, "location-refresh-menuitem");
    g_signal_connect (menuitem, "activate", G_CALLBACK (on_refresh_location), location_chooser);
    menuitem = glade_xml_get_widget (ui, "location-edit-menuitem");
    g_signal_connect (menuitem, "activate", G_CALLBACK (on_edit_location), location_chooser);
    menuitem = glade_xml_get_widget (ui, "location-remove-menuitem");
    g_signal_connect (menuitem, "activate", G_CALLBACK (on_remove_location), location_chooser);

    /* load player into the workspace switcher */
    player = undertow_player_new ();
    undertow_workspace_switcher_add_page (UNDERTOW_WORKSPACE_SWITCHER (switcher),
                                          "player://",
                                          player);

    /* load pages into the workspace switcher */
    gtk_tree_model_foreach (GTK_TREE_MODEL (model),
                            (GtkTreeModelForeachFunc) add_location_page,
                            switcher);

    return window;
}

void
undertow_main_window_destroy (void)
{
    g_object_unref (ui);
    g_object_unref (model);
}
