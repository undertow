#include <gtk/gtk.h>
#include <gconf/gconf-client.h>
#include <gdk/gdkkeysyms.h>
#include <libundertow.h>

#include "undertow-main-window.h"
#include "undertow-first-run.h"

int
main (int argc, char *argv[])
{
    GConfClient *gconf;
    gchar *path, *data_dir;
    GError *error = NULL;
    GtkWidget *window;

    /* initialize threading.  it's important to do this first */
    g_thread_init (NULL);
    gdk_threads_init ();

    /* initialize libraries */
    gtk_init (&argc, &argv);

    /* load application settings */
    gconf = gconf_client_get_default ();
    gconf_client_add_dir (gconf, "/apps/undertow", GCONF_CLIENT_PRELOAD_ONELEVEL, &error);
    if (error)
        g_error ("Failed to read settings: %s", error->message);

    /* check to see if the application directory exists, and possibly run the setup assistant */
    data_dir = undertow_first_run_check (gconf);

    /* set the database path and download directory */
    path = g_build_path ("/", data_dir, "undertowdb", NULL);
    lut_option_set_database_path (path);
    g_free (path);
    path = g_build_path ("/", data_dir, "videos", NULL);
    lut_option_set_download_directory (path);
    g_free (path);
    g_free (data_dir);

    /* now it is safe to start up the undertow dispatcher */
    lut_main_start ();

    /* create the main window */
    window = undertow_main_window_create ();
    gtk_widget_show_all (window);

    /* 
     * pass control to gtk.  we need to protect gtk_main() with the GDK
     * lock because the first thing gtk_main() does is unlock it.
     */
    gdk_threads_enter ();
    gtk_main ();
    gdk_threads_leave ();

    undertow_main_window_destroy ();
    lut_main_stop ();
    return 0;
}
