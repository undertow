#include <gtk/gtk.h>

#include "undertow-workspace-switcher.h"

static GtkContainerClass *parent_class = NULL;

static void
switcher_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
    UndertowWorkspaceSwitcher *switcher = UNDERTOW_WORKSPACE_SWITCHER (widget);
    GHashTableIter iter;
    GtkWidget *page;

    requisition->width = -1;
    requisition->height = -1;

    g_hash_table_iter_init (&iter, switcher->pages);
    while (g_hash_table_iter_next (&iter, NULL, (gpointer *) &page)) {
        if (GTK_WIDGET_REALIZED (page))
            gtk_widget_size_request (page, requisition);
    }
}

static void
switcher_size_allocate (GtkWidget *widget, GtkAllocation *alloc)
{
    UndertowWorkspaceSwitcher *switcher = UNDERTOW_WORKSPACE_SWITCHER (widget);
    GHashTableIter iter;
    GtkWidget *page;

    GTK_WIDGET_CLASS (parent_class)->size_allocate (widget, alloc);
    g_hash_table_iter_init (&iter, switcher->pages);
    while (g_hash_table_iter_next (&iter, NULL, (gpointer *) &page)) {
        if (GTK_WIDGET_REALIZED (page))
            gtk_widget_size_allocate (page, alloc);
    }
}

static void
switcher_realize (GtkWidget *widget)
{
    GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);
    widget->window = gtk_widget_get_parent_window (widget);
    g_object_ref (widget->window);
    widget->style = gtk_style_attach (widget->style, widget->window);
    gtk_widget_queue_draw (widget);
}

static void
switcher_map (GtkWidget *widget)
{
    GTK_WIDGET_CLASS (parent_class)->map (widget);
    gtk_widget_queue_resize (widget);
}

static void
switcher_add (GtkContainer *container, GtkWidget *widget)
{
    ;
}

static void
switcher_remove (GtkContainer *container, GtkWidget *widget)
{
    ;
}

static void
switcher_forall (GtkContainer *     container,
                 gboolean           include_internals,
                 GtkCallback        callback,
                 gpointer           callback_data)
{
    UndertowWorkspaceSwitcher *switcher = UNDERTOW_WORKSPACE_SWITCHER (container);
    GHashTableIter iter;
    GtkWidget *page;

    g_hash_table_iter_init (&iter, switcher->pages);
    while (g_hash_table_iter_next (&iter, NULL, (gpointer *) &page))
        callback (page, callback_data);
    //g_debug ("switcher_forall");
}

static GType
switcher_child_type (GtkContainer *container)
{
    return GTK_TYPE_WIDGET;
}

static void
switcher_dispose (UndertowWorkspaceSwitcher *switcher)
{
    if (switcher->disposed)
        return;
    G_OBJECT_CLASS (parent_class)->dispose (G_OBJECT (switcher));
    g_hash_table_destroy (switcher->pages);
    switcher->disposed = TRUE;
}

static void
switcher_init (UndertowWorkspaceSwitcher *          switcher,
               UndertowWorkspaceSwitcherClass *     klass)
{
    GTK_WIDGET_SET_FLAGS (switcher, GTK_NO_WINDOW);

    switcher->pages =
        g_hash_table_new_full (g_str_hash,
                               g_str_equal,
                               g_free,
                               NULL);
    switcher->blank = gtk_label_new ("hello, world!");
    switcher->current = switcher->blank;
    gtk_widget_freeze_child_notify (switcher->blank);
    gtk_widget_set_parent (switcher->blank, GTK_WIDGET (switcher));
    gtk_widget_set_child_visible (switcher->current, TRUE);
    gtk_widget_thaw_child_notify (switcher->blank);
}

static void
switcher_class_init (UndertowWorkspaceSwitcherClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
    GtkContainerClass *container_class = GTK_CONTAINER_CLASS (klass);

    parent_class = g_type_class_peek_parent (klass);

    object_class->dispose = 
        (GObjectFinalizeFunc) switcher_dispose;

    widget_class->size_request = switcher_size_request;
    widget_class->size_allocate = switcher_size_allocate;
    widget_class->realize = switcher_realize;
    widget_class->map = switcher_map;

    container_class->child_type = switcher_child_type;
    container_class->add = switcher_add;
    container_class->remove = switcher_remove;
    container_class->forall = switcher_forall;
}

GType 
undertow_workspace_switcher_get_type (void)
{
    static GType type = 0;

    if (!type) {
        static const GTypeInfo info = {
            sizeof (UndertowWorkspaceSwitcherClass),
            NULL,
            NULL,
            (GClassInitFunc) switcher_class_init,
            NULL,
            NULL,
            sizeof (UndertowWorkspaceSwitcher),
            0,
            (GInstanceInitFunc) switcher_init,
            NULL
        };
        type = g_type_register_static (GTK_TYPE_CONTAINER, 
                                       "UndertowWorkspaceSwitcher",
                                       &info, 0);
    }
    return type;
}

GtkWidget *
undertow_workspace_switcher_new (void)
{
    return GTK_WIDGET (g_object_new (UNDERTOW_TYPE_WORKSPACE_SWITCHER, NULL));
}

void
undertow_workspace_switcher_set_page (UndertowWorkspaceSwitcher *       switcher,
                                      const gchar *                     url)
{
    GtkWidget *page;

    if (url) {
        page = g_hash_table_lookup (switcher->pages, url);
        g_return_if_fail (page != NULL);
    }
    else
        page = switcher->blank;
    if (switcher->current)
        gtk_widget_set_child_visible (switcher->current, FALSE);
    gtk_widget_set_child_visible (page, TRUE);
    switcher->current = page;
    gtk_widget_queue_resize (page); // necessary?
    g_debug ("set switcher page to %s", url);
}

void
undertow_workspace_switcher_add_page (UndertowWorkspaceSwitcher *       switcher,
                                      const gchar *                     url,
                                      GtkWidget *                       page)
{
    g_return_if_fail (url != NULL);

    g_hash_table_replace (switcher->pages, g_strdup (url), g_object_ref (page));
    gtk_widget_freeze_child_notify (page);
    gtk_widget_set_parent (page, GTK_WIDGET (switcher));
    gtk_widget_set_child_visible (page, FALSE);
    gtk_widget_thaw_child_notify (page);
}

void
undertow_workspace_switcher_remove_page (UndertowWorkspaceSwitcher *    switcher,
                                         const gchar *                  url)
{
    g_hash_table_remove (switcher->pages, url);
}

