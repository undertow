#ifndef UNDERTOW_REFLOW_LABEL_H
#define UNDERTOW_REFLOW_LABEL_H

#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

#define UNDERTOW_TYPE_REFLOW_LABEL             (undertow_reflow_label_get_type())
#define UNDERTOW_REFLOW_LABEL(obj)             \
    (G_TYPE_CHECK_INSTANCE_CAST((obj), UNDERTOW_TYPE_REFLOW_LABEL, UndertowReflowLabel))
#define UNDERTOW_REFLOW_LABEL_CLASS(klass)     \
    (G_TYPE_CHECK_CLASS_CAST((klass), UNDERTOW_TYPE_REFLOW_LABEL, UndertowReflowLabelClass))
#define UNDERTOW_IS_REFLOW_LABEL(obj)          \
    (G_TYPE_CHECK_INSTANCE_TYPE((obj), UNDERTOW_TYPE_REFLOW_LABEL))
#define UNDERTOW_IS_REFLOW_LABEL_CLASS(klass)  \
    (G_TYPE_CHECK_CLASS_TYPE((klass), UNDERTOW_TYPE_REFLOW_LABEL))
#define UNDERTOW_REFLOW_LABEL_GET_CLASS(obj)   \
    (G_TYPE_INSTANCE_GET_CLASS((obj), UNDERTOW_TYPE_REFLOW_LABEL, UndertowReflowLabelClass))

typedef struct {
    GtkDrawingArea parent;
    gboolean disposed;
    gchar *text;
    PangoLayout *layout;
} UndertowReflowLabel;

typedef struct {
    GtkDrawingAreaClass parent_class;
} UndertowReflowLabelClass;

GType
undertow_reflow_label_get_type (void);

GtkWidget *
undertow_reflow_label_new (const gchar *text);

GtkWidget *
undertow_reflow_label_new_with_markup (const gchar *markup);

#endif
