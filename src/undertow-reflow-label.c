#include <glib.h>
#include <glib-object.h>
#include <gtk/gtk.h>

#include "undertow-reflow-label.h"

enum { LABEL_TEXT = 1, LABEL_MARKUP };

static GtkDrawingAreaClass *parent_class = NULL;

static void
label_size_request (GtkWidget *widget, GtkRequisition *requisition)
{
    requisition->width = 1;
    requisition->height = 1;
    /*
    g_debug ("label_size_request: requisition=(%i, %i)",
             requisition->width,
             requisition->height);
             */
}

static void
label_size_allocate (GtkWidget *widget, GtkAllocation *alloc)
{
    UndertowReflowLabel *label = UNDERTOW_REFLOW_LABEL (widget);

    GTK_WIDGET_CLASS (parent_class)->size_allocate (widget, alloc);

    /*g_debug ("label_size_allocate: old alloc=(%i, %i)", alloc->width, alloc->height); */
    pango_layout_set_width (label->layout, alloc->width * PANGO_SCALE);
    pango_layout_get_pixel_size (label->layout, NULL, &(alloc->height));
    /*g_debug ("label_size_allocate: new alloc=(%i, %i)", alloc->width, alloc->height);*/
    /*
    widget->requisition.width = alloc->width;
    widget->requisition.height = alloc->height;
    */
    gtk_widget_set_size_request (widget, alloc->width, alloc->height);
}

static void
label_realize (GtkWidget *widget)
{
    GTK_WIDGET_SET_FLAGS (widget, GTK_REALIZED);
    widget->window = gtk_widget_get_parent_window (widget);
    g_object_ref (widget->window);
    widget->style = gtk_style_attach (widget->style, widget->window);
}

static gboolean
label_expose (GtkWidget *widget, GdkEventExpose *ev)
{
    UndertowReflowLabel *label = UNDERTOW_REFLOW_LABEL (widget);

    if (label->layout == NULL)
        return FALSE;
    gtk_paint_layout (widget->style,
                      widget->window,
                      GTK_WIDGET_STATE (widget),
                      FALSE,
                      NULL,
                      widget,
                      NULL,
                      widget->allocation.x, widget->allocation.y,
                      label->layout);
    return FALSE;
}

static void
label_get_property (UndertowReflowLabel *       label,
                    guint                       property_id,
                    GValue *                    value,
                    GParamSpec *                spec)
{
    switch (property_id) {
        case LABEL_TEXT:
        case LABEL_MARKUP:
            g_value_set_string (value, label->text);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (G_OBJECT (label), property_id, spec);
            break;
    }
}

static void 
label_set_property (UndertowReflowLabel *       label,
                    guint                       property_id,
                    const GValue *              value,
                    GParamSpec *                spec)
{
    switch (property_id) {
        case LABEL_TEXT:
            if (label->text)
                g_free (label->text);
            label->text = g_value_dup_string (value);
            pango_layout_set_text (label->layout, label->text, -1);
            break;
        case LABEL_MARKUP:
            if (label->text)
                g_free (label->text);
            label->text = g_value_dup_string (value);
            pango_layout_set_markup (label->layout, label->text, -1);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (G_OBJECT (label), property_id, spec);
            break;
    }
}

static void
label_dispose (UndertowReflowLabel *label)
{
    if (label->disposed)
        return;
    G_OBJECT_CLASS (parent_class)->dispose (G_OBJECT (label));
    g_object_unref (label->layout);
    label->disposed = TRUE;
}

static void
label_init (GtkWidget *widget)
{
    UndertowReflowLabel *label = UNDERTOW_REFLOW_LABEL (widget);

    GTK_WIDGET_SET_FLAGS (widget, GTK_NO_WINDOW);
    label->layout = gtk_widget_create_pango_layout (widget, NULL);
}

static void
label_class_init (UndertowReflowLabelClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);
    GParamSpec *pspec;

    parent_class = g_type_class_peek_parent (klass);

    object_class->get_property =
      (GObjectGetPropertyFunc) label_get_property;
    object_class->set_property =
      (GObjectSetPropertyFunc) label_set_property;
    object_class->dispose =
        (GObjectFinalizeFunc) label_dispose;
    widget_class->size_request = label_size_request;
    widget_class->size_allocate = label_size_allocate;
    widget_class->realize = label_realize;
    widget_class->expose_event = label_expose;

    pspec = g_param_spec_string ("text", "text",
                                 "Get/set the label text",
                                 NULL,
                                 G_PARAM_READWRITE);
    g_object_class_install_property (object_class, LABEL_TEXT, pspec);
    pspec = g_param_spec_string ("markup", "markup",
                                 "Get/set the label markup",
                                 NULL,
                                 G_PARAM_READWRITE);
    g_object_class_install_property (object_class, LABEL_MARKUP, pspec);
}

GType
undertow_reflow_label_get_type (void)
{
    static GType type = 0;

    if (!type) {
        static const GTypeInfo label_info = {
            sizeof (UndertowReflowLabelClass),
            NULL,
            NULL,
            (GClassInitFunc) label_class_init,
            NULL,
            NULL,
            sizeof (UndertowReflowLabel),
            0,
            (GInstanceInitFunc) label_init,
            NULL
        };
        type = g_type_register_static (GTK_TYPE_DRAWING_AREA, 
                                       "UndertowReflowLabel",
                                       &label_info, 0);
    }
    return type;
}

GtkWidget *
undertow_reflow_label_new (const gchar *text)
{
    return (GtkWidget *)
        g_object_new (UNDERTOW_TYPE_REFLOW_LABEL, "text", text, NULL);
}

GtkWidget *
undertow_reflow_label_new_with_markup (const gchar *markup)
{
    return (GtkWidget *)
        g_object_new (UNDERTOW_TYPE_REFLOW_LABEL, "markup", markup, NULL);
}
