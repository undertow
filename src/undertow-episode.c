#include <glib.h>
#include <glib/gstdio.h>
#include <glib-object.h>
#include <gtk/gtk.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <libundertow.h>

#include "undertow-episode.h"
#include "undertow-channel.h"
#include "undertow-g-error.h"

enum {
    EPISODE_CHANNEL_URL = 1,
    EPISODE_CONTENT_URL,
    EPISODE_TITLE,
    EPISODE_DESCRIPTION,
    EPISODE_PUBLISHED
};

enum {
    DOWNLOAD_PROGRESS_SIGNAL,
    STATE_CHANGED_SIGNAL,
    DOWNLOAD_ERROR_SIGNAL,
    N_EPISODE_SIGNALS
};
static guint signals[N_EPISODE_SIGNALS];

static GObjectClass *parent_class = NULL;

static void
episode_get_property (UndertowEpisode *         episode,
                      guint                     property_id,
                      GValue *                  value,
                      GParamSpec *              spec)
{
    switch (property_id) {
        case EPISODE_CHANNEL_URL:
            g_value_set_string (value, episode->channel_url);
            break;
        case EPISODE_CONTENT_URL:
            g_value_set_string (value, episode->content_url);
            break;
        case EPISODE_TITLE:
            g_value_set_string (value, episode->title);
            break;
        case EPISODE_DESCRIPTION:
            g_value_set_string (value, episode->description);
            break;
        case EPISODE_PUBLISHED:
            g_value_set_uint (value, episode->published);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (G_OBJECT (episode), property_id, spec);
            break;
    }
}

static void 
episode_set_property (UndertowEpisode *         episode,
                      guint                     property_id,
                      const GValue *            value,
                      GParamSpec *              spec)
{
    switch (property_id) {
        case EPISODE_CHANNEL_URL:
            episode->channel_url = g_value_dup_string (value);
            break;
        case EPISODE_CONTENT_URL:
            episode->content_url = g_value_dup_string (value);
            break;
        case EPISODE_TITLE:
            episode->title = g_value_dup_string (value);
            break;
        case EPISODE_DESCRIPTION:
            episode->description = g_value_dup_string (value);
            break;
        case EPISODE_PUBLISHED:
            episode->published = g_value_get_uint (value);
            break;
        default:
            G_OBJECT_WARN_INVALID_PROPERTY_ID (G_OBJECT (episode), property_id, spec);
            break;
    }
}

static void
episode_dispose (UndertowEpisode *episode)
{
    if (episode->disposed)
        return;
    g_debug ("episode_dispose: %s", episode->content_url);
    if (episode->channel_url)
        g_free (episode->channel_url);
    if (episode->content_url)
        g_free (episode->content_url);
    if (episode->title)
        g_free (episode->title);
    if (episode->description)
        g_free (episode->description);
    if (episode->file_path)
        g_free (episode->file_path);
    if (episode->file_type)
        g_free (episode->file_type);
    /* if episode is downloading, cancel the download */
    episode->disposed = TRUE;
    G_OBJECT_CLASS (parent_class)->dispose (G_OBJECT (episode));
}

static void
episode_class_init (UndertowEpisodeClass *klass)
{
    GObjectClass *object_class = G_OBJECT_CLASS (klass);
    GParamSpec *pspec;

    parent_class = g_type_class_peek_parent (klass);

    object_class->get_property =
        (GObjectGetPropertyFunc) episode_get_property;
    object_class->set_property =
        (GObjectSetPropertyFunc) episode_set_property;
    object_class->dispose =
        (GObjectFinalizeFunc) episode_dispose;

    pspec = g_param_spec_string ("channel-url", "episode channel URL",
                                 "Get/set the episode channel",
                                 NULL,
                                 G_PARAM_READWRITE|G_PARAM_CONSTRUCT_ONLY);
    g_object_class_install_property (object_class, EPISODE_CHANNEL_URL, pspec);
    pspec = g_param_spec_string ("content-url", "episode content URL",
                                 "Get the episode content URL",
                                 NULL,
                                 G_PARAM_READWRITE|G_PARAM_CONSTRUCT_ONLY);
    g_object_class_install_property (object_class, EPISODE_CONTENT_URL, pspec);
    pspec = g_param_spec_string ("title", "episode title",
                                 "Get the episode title",
                                 NULL,
                                 G_PARAM_READWRITE|G_PARAM_CONSTRUCT_ONLY);
    g_object_class_install_property (object_class, EPISODE_TITLE, pspec);
    pspec = g_param_spec_string ("description", "episode description",
                                 "Get the episode description",
                                 NULL,
                                 G_PARAM_READWRITE|G_PARAM_CONSTRUCT_ONLY);
    g_object_class_install_property (object_class, EPISODE_DESCRIPTION, pspec);
    pspec = g_param_spec_uint ("published", "episode publication date",
                               "Get the episode publication date",
                               0, G_MAXUINT, 0,
                               G_PARAM_READWRITE|G_PARAM_CONSTRUCT_ONLY);
    g_object_class_install_property (object_class, EPISODE_PUBLISHED, pspec);

    signals[DOWNLOAD_PROGRESS_SIGNAL] =
      g_signal_new ("download-progress",
                    G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST|G_SIGNAL_NO_RECURSE|G_SIGNAL_NO_HOOKS,
                    G_STRUCT_OFFSET (UndertowEpisodeClass, download_progress),
                    NULL, NULL,
                    undertow_cclosure_VOID__UINT_UINT_FLOAT_FLOAT,
                    G_TYPE_NONE, 4, G_TYPE_UINT, G_TYPE_UINT, G_TYPE_FLOAT, G_TYPE_FLOAT);
    signals[STATE_CHANGED_SIGNAL] =
      g_signal_new ("state-changed",
                    G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST|G_SIGNAL_NO_RECURSE|G_SIGNAL_NO_HOOKS,
                    G_STRUCT_OFFSET (UndertowEpisodeClass, state_changed),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__INT,
                    G_TYPE_NONE, 1, G_TYPE_INT);
    signals[DOWNLOAD_ERROR_SIGNAL] =
      g_signal_new ("download-error",
                    G_TYPE_FROM_CLASS (klass),
                    G_SIGNAL_RUN_LAST|G_SIGNAL_NO_RECURSE|G_SIGNAL_NO_HOOKS,
                    G_STRUCT_OFFSET (UndertowEpisodeClass, download_error),
                    NULL, NULL,
                    g_cclosure_marshal_VOID__POINTER,
                    G_TYPE_NONE, 1, G_TYPE_POINTER);
}

GType
undertow_episode_get_type (void)
{
    static GType type = 0;

    if (!type) {
        static const GTypeInfo episode_info = {
            sizeof (UndertowEpisodeClass),
            NULL,
            NULL,
            (GClassInitFunc) episode_class_init,
            NULL,
            NULL,
            sizeof (UndertowEpisode),
            0,
            (GInstanceInitFunc) NULL,
            NULL
        };
        type = g_type_register_static (G_TYPE_OBJECT, 
                                       "UndertowEpisode",
                                       &episode_info, 0);
    }
    return type;
}

UndertowDownloadState
undertow_episode_get_state (UndertowEpisode *episode)
{
    lut_episode *e;
    lut_download *download;
    lut_errno retval;
    UndertowDownloadState state;

    retval = lut_get_episode (episode->channel_url, episode->content_url, &e);
    if (retval == LUT_OK) {
        if (e->file_path == NULL)
            state = UNDERTOW_DOWNLOAD_STATE_NOT_RUNNING;
        else {
            retval = lut_get_download (e->file_path, &download);
            if (retval == LUT_OK) {
                switch (download->state) {
                    case LUT_DOWNLOAD_STATE_NOT_RUNNING:
                        state = UNDERTOW_DOWNLOAD_STATE_NOT_RUNNING;
                        break;
                    case LUT_DOWNLOAD_STATE_RUNNING:
                        state = UNDERTOW_DOWNLOAD_STATE_RUNNING;
                        break;
                    case LUT_DOWNLOAD_STATE_FINISHED:
                        state = UNDERTOW_DOWNLOAD_STATE_FINISHED;
                        break;
                }
                lut_download_free (download);
            }
            else
                g_debug ("lut_get_download failed: %s", lut_strerror (retval));
        }
        lut_episode_free (e);
    }
    else
        g_debug ("lut_get_episode failed: %s", lut_strerror (retval));
    return state;
}

static void
on_download_progress (size_t    length,
                      size_t    transferred,
                      float     fraction,
                      float     speed,
                      void *    user_data)
{
    UndertowEpisode *episode;

    gdk_threads_enter ();

    episode = UNDERTOW_EPISODE (user_data);
    g_signal_emit (episode,
                   signals[DOWNLOAD_PROGRESS_SIGNAL],
                   0,
                   (guint) length, (guint) transferred, (gfloat) fraction, (gfloat) speed);

    gdk_flush ();
    gdk_threads_leave ();
}

static void
on_download_completed (lut_errno retval, void *user_data)
{
    UndertowEpisode *episode;
   
    gdk_threads_enter ();

    episode = UNDERTOW_EPISODE (user_data);
    if (retval != LUT_OK) {
        GError *error = g_error_new (UNDERTOW_G_ERROR, retval, lut_strerror (retval));
        g_signal_emit (episode,
                       signals[DOWNLOAD_ERROR_SIGNAL],
                       0,
                       error);
        g_signal_emit (episode,
                       signals[STATE_CHANGED_SIGNAL],
                       0,
                       UNDERTOW_DOWNLOAD_STATE_NOT_RUNNING);
    }
    else {
        g_signal_emit (episode,
                       signals[STATE_CHANGED_SIGNAL],
                       0,
                       UNDERTOW_DOWNLOAD_STATE_FINISHED);
    }
    g_object_unref (episode);

    gdk_flush ();
    gdk_threads_leave ();
}

gboolean
undertow_episode_start_download (UndertowEpisode *episode, GError **error)
{
    gchar *channel_url;
    lut_episode *e;
    lut_errno retval;
   
    retval = lut_get_episode (episode->channel_url, episode->content_url, &e);
    if (retval != LUT_OK) {
        g_free (channel_url);
        return FALSE;
    }
    g_object_ref (episode);
    retval = lut_start_download (e,
                                 &(episode->download),
                                 on_download_progress,
                                 on_download_completed,
                                 episode);
    if (retval != LUT_OK) {
        g_object_unref (episode);
        g_critical ("undertow_episode_start_download: %s", lut_strerror (retval));
        return FALSE;
    }
    g_signal_emit (episode,
                   signals[STATE_CHANGED_SIGNAL],
                   0,
                   UNDERTOW_DOWNLOAD_STATE_RUNNING);
    return TRUE;
}
