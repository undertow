#!/bin/sh

echo "Bootstrapping undertow build environment..."
set -x
aclocal-1.7
autoconf
libtoolize --copy
automake-1.7 --add-missing --copy --gnu
